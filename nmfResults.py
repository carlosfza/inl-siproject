import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Button
import cubeData as cd
import decomposer


class Formatter(object):
    def __init__(self, im):
        self.im = im

    def __call__(self, x, y):
        #z = self.im.get_array()[int(y), int(x)]
        z = self.im.get_array()[int(y), int(x)]
        return 'x={}, y={}, H[x, y]={:.01f}'.format(int(x), int(y), z)


class Index:
    def __init__(self, W, H, eOrigin=0, eScale=1):
        self.W = W
        self.H = H
        self.index = 0
        self.size = np.size(W, 1)
        self.scale = 'relative'
        self.relativeMax = np.zeros((np.size(H, 0)))
        self.relativeMin = np.zeros((np.size(H, 0)))
        self.absoluteMax = H.max()
        self.absoluteMin = H.min()

        for i in xrange(0, np.size(self.H, 0)):
            self.relativeMax[i] = self.H[i, :, :].max()
            self.relativeMin[i] = self.H[i, :, :].min()

        #Interface Creation
        #Creation of the first window, Figure1 contains the spectrum components (W)
        self.fig = plt.figure('1')
        self.ax = plt.subplot()
        plt.ylabel('counts')
        plt.xlabel('energy (eV)')

        plt.subplots_adjust(bottom=0.2)
        horizontalScale = np.linspace(eOrigin, eOrigin + np.size(self.W, 0) * eScale, np.size(self.W, 0))
        self.l, = self.ax.plot(horizontalScale, self.W[:, 0], lw=2)
        self.fig.suptitle('W - index {} of {}'.format(0, np.size(self.W, 1) - 1))

        #Criacao dos botoes Next e Prev Fig1
        axprev = plt.axes([0.7, 0.05, 0.1, 0.065])
        axnext = plt.axes([0.81, 0.05, 0.1, 0.065])
        self.bnext = Button(axnext, 'Next')
        self.bnext.on_clicked(self.next)
        self.bprev = Button(axprev, 'Previous')
        self.bprev.on_clicked(self.prev)

        #Criacao dos botoes Absolute e Relative
        axRelative = plt.axes([0.1, 0.05, 0.1, 0.065])
        axAbsolute = plt.axes([0.21, 0.05, 0.1, 0.065])
        self.bRelative = Button(axRelative, 'Relative')
        self.bRelative.on_clicked(self.relative)
        self.bAbsolute = Button(axAbsolute, 'Absolute')
        self.bAbsolute.on_clicked(self.absolute)

        #Def fig 2, Figure 2 contains H, which is the weights of the W components
        self.fig2 = plt.figure('2')
        self.ax2 = plt.subplot()

        self.im = self.ax2.imshow(self.H[0, :, :], interpolation='none')
        self.ax2.format_coord = Formatter(self.im)
        self.im.set_clim(self.relativeMin[0], self.relativeMax[0])
        self.collorBar = self.fig2.colorbar(self.im)

        plt.show()

    def next(self, arg):
        plt.figure('1')
        self.index += 1
        i = self.index % self.size
        self.l.set_ydata(self.W[:, i])
        self.fig.suptitle('W - index {} of {}'.format(i, self.size - 1))
        plt.draw()

        plt.figure('2')
        self.fig2.suptitle('H - index {} of {}'.format(i, self.size - 1))
        self.im = plt.imshow(self.H[i,:,:], interpolation='none')
        self.ax2.format_coord = Formatter(self.im)
        #plt.format_coord = Formatter(self.H)
        if self.scale == 'relative':
            self.im.set_clim(self.relativeMin[i], self.relativeMax[i])
            self.collorBar.set_clim(self.relativeMin[i], self.relativeMax[i])
        elif self.scale == 'absolute':
            self.im.set_clim(self.absoluteMin, self.absoluteMax)
            self.collorBar.set_clim(self.absoluteMin, self.absoluteMax)
        self.collorBar.draw_all()

        plt.draw()

    def prev(self, agr):
        plt.figure('1')
        self.index -= 1
        i = self.index % self.size
        self.l.set_ydata(self.W[:, i])
        self.fig.suptitle('W - index {} of {}'.format(i, self.size - 1))
        plt.draw()

        plt.figure('2')
        self.fig2.suptitle('H - index {} of {}'.format(i, self.size - 1))
        self.im = self.ax2.imshow(self.H[i, :, :], interpolation='none')
        self.ax2.format_coord = Formatter(self.im)
        if self.scale == 'relative':
            self.im.set_clim(self.relativeMin[i], self.relativeMax[i])
            self.collorBar.set_clim(self.relativeMin[i], self.relativeMax[i])
        elif self.scale == 'absolute':
            self.im.set_clim(self.absoluteMin, self.absoluteMax)
            self.collorBar.set_clim(self.absoluteMin, self.absoluteMax)
        self.collorBar.draw_all()
        plt.draw()

    def relative(self, agr):
        if self.scale == 'relative':
            return
        else:
            i = self.index % self.size
            self.scale = 'relative'

            plt.figure('2')
            self.im = self.ax2.imshow(self.H[i, :, :], interpolation='none')
            self.ax2.format_coord = Formatter(self.im)
            self.im.set_clim(self.relativeMin[i], self.relativeMax[i])
            self.collorBar.set_clim(self.relativeMin[i], self.relativeMax[i])
            self.collorBar.draw_all()
            plt.draw()

    def absolute(self, agr):
        if self.scale == 'absolute':
            return
        else:
            i = self.index % self.size
            self.scale = 'absolute'

            plt.figure('2')
            self.im = self.ax2.imshow(self.H[i, :, :], interpolation='none')
            self.ax2.format_coord = Formatter(self.im)
            self.im.set_clim(self.absoluteMin, self.absoluteMax)
            self.collorBar.set_clim(self.absoluteMin, self.absoluteMax)
            self.collorBar.draw_all()
            plt.draw()

