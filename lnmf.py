import numpy as np
import collections


####################################
#This is a Python adaptation of the Local Non-Negative Matrix Factorization, originally made by Stan Li
#This module was based on the Matlab LNMF implementation by Patrik Hoyer. Hoyer's code can be found at https://github.com/aludnam/MATLAB/tree/master/nmfpack
#Hoye's publication on the topic can be found at: http://arxiv.org/pdf/cs/0408058.pdf
####################################


class LNMF:

    def __init__(self):
        pass

    @staticmethod
    def doLNMF(dataArray, W, H, alpha, beta, numIt):

        #Check that we do not have non-negative data
        if np.min(dataArray) < 0:
            #If we have negative data we add the minimium in order to obtain an array with only positive values
            nz = np.zeros((np.size(dataArray, 0), np.size(dataArray, 1)))
            nz.fill(np.min(dataArray))
            dataArray = dataArray - nz

        #Globally rescale data to avoid potential underflow, overflow
        dataArray /= np.max(dataArray)

        #dimensions
        vdim = np.size(dataArray, 0)
        #samples = np.size(dataArray, 1)
        if np.size(W, 1) == np.size(H, 0):
            rdim = np.size(W, 1)
        else:
            return

        #Make sure W has unit sum columns
        W = W / (np.ones((vdim, 1)) * np.array(np.sum(np.matrix(W), 0)))

        #calculate initial objective
        #objHistory = np.sum(dataArray * np.log(dataArray / W.dot(H)) - dataArray + W.dot(H)) + alpha * np.sum(W.T.dot(W)) - beta * sum(np.diag(H.dot(H.T)))

        #Start iteration
        for i in xrange(0, numIt):

            #Show progress
            print 'iteration number ',
            print i

            #Update rules, finally
            Vc = dataArray / (W.dot(H))
            H = np.sqrt(H * (W.T.dot(Vc)))

            s1 = np.array(np.sum(np.matrix(W), 1))
            r1 = np.array(np.tile(np.matrix(s1), (1, rdim)))

            s2 = np.array(np.sum(np.matrix(H.T), 0))
            r2 = np.array(np.tile(np.matrix(s2), (vdim, 1)))

            W = W * (Vc.dot(H.T)) / (r1 + r2)
            W = W / (np.ones((vdim, 1)).dot(np.array(np.sum(np.matrix(W), 0))))

            #Calculate objective
            #newobj = sum(dataArray * np.log( dataArray / (W.dot(H))) - dataArray + W.dot(H))
            #newobj += + alpha * np.sum(W.T.dot(W)) - beta * np.sum(np.diag(H.dot(H.T)))
        nmfResultTuple = collections.namedtuple('nmfResultTuple', ['dataArray', 'W', 'H'])
        return nmfResultTuple(dataArray, W, H)
