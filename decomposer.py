import pgnmf
import lnmf
import numpy as np
import nmfResults
import cubeData
import matplotlib.pyplot as plt
from matplotlib.widgets import Button


class Decomposer:
    def __init__(self):
        self.pgnmfDecomp = pgnmf.PGNMF()
        self.lnmfDecomp = lnmf.LNMF()

    def lnmfDataSetRandom(self, dataCube, rank, numIt):
        #Get the pixelMatrix in the format of NxM (size N of each sample, M total samples)
        dataArray = dataCube.returnAgregatedMatrix()

        #Teste, processar o dataArray
        dataArray = dataArray * 1000
        dataArray = dataArray + 1

        #Calculate the initial W and H
        vdim = np.size(dataArray, 0)
        samples = np.size(dataArray, 1)
        W = np.absolute(np.random.rand(vdim, rank))
        H = np.absolute(np.random.rand(rank, samples))

        #Perform the Local NMF
        res = self.lnmfDecomp.doLNMF(dataArray, W, H, 0, 0, numIt)
        print 'LNMF Finished. Sum of error: {}'.format(np.sum(np.absolute(dataArray - res.W.dot(res.H))))
        window = nmfResults.Index(res.W, np.reshape(res.H, (rank, dataCube.mHeight, dataCube.mWidth)))

    def pgDataSetRandom(self, dataCube, rank, numIt):
        #Get the pixelMatrix in the format of NxM (size N of each sample, M total samples)
        dataArray = dataCube.returnAgregatedMatrix()

        #Calculate the initial W and H
        vdim = np.size(dataArray, 0)
        samples = np.size(dataArray, 1)
        W = np.absolute(np.random.rand(vdim, rank))
        H = np.absolute(np.random.rand(rank, samples))

        #Perform the Projected Gradient NMF
        res = self.pgnmfDecomp.doPgNMF(dataArray, W, H, 0, 0, numIt)
        print 'PGNMF Finished. Sum of error: {}'.format(np.sum(np.absolute(dataArray - res.W.dot(res.H))))
        window = nmfResults.Index(res.W, np.reshape(res.H, (rank, dataCube.mHeight, dataCube.mWidth)))

    def pgDataSetRandomReduced(self, dataCube, rank, numIt):
        #Get the pixelMatrix in the format of NxM (size N of each sample, M total samples)
        dataArray = dataCube.returnAgregatedReduced()

        #Calculate the initial W and H
        vdim = np.size(dataArray, 0)
        samples = np.size(dataArray, 1)
        W = np.absolute(np.random.rand(vdim, rank))
        H = np.absolute(np.random.rand(rank, samples))

        #Perform the Projected Gradient NMF
        res = self.pgnmfDecomp.doPgNMF(dataArray, W, H, 0, 0, numIt)
        print 'PGNMF Finished. Sum of error: {}'.format(np.sum(np.absolute(dataArray - res.W.dot(res.H))))
        window = nmfResults.Index(res.W, np.reshape(res.H, (rank, dataCube.reducedHeight, dataCube.reducedWidth)))

    def lnmfDataSetRandomReduced(self, dataCube, rank, numIt):
        #Get the pixelMatrix in the format of NxM (size N of each sample, M total samples)
        dataArray = dataCube.returnAgregatedReduced()

        #Teste, processar o dataArray
        dataArray = dataArray * 1000
        dataArray = dataArray + 1

        #Calculate the initial W and H
        vdim = np.size(dataArray, 0)
        samples = np.size(dataArray, 1)
        W = np.absolute(np.random.rand(vdim, rank))
        H = np.absolute(np.random.rand(rank, samples))

        #Perform the Projected Gradient NMF
        res = self.lnmfDecomp.doLNMF(dataArray, W, H, 0, 0, numIt)
        print 'PGNMF Finished. Sum of error: {}'.format(np.sum(np.absolute(dataArray - res.W.dot(res.H))))

        H_ = np.zeros((rank, dataCube.reducedHeight, dataCube.reducedWidth))
        #Reshape the H matrix into an image
        for x in xrange(0, dataCube.reducedHeight):
            for y in xrange(0, dataCube.reducedWidth):
                aux = res.H[:, x * dataCube.reducedWidth + y]
                H_[:, x , y] = aux
        window = nmfResults.Index(res.W, np.reshape(res.H, (rank, dataCube.reducedHeight, dataCube.reducedWidth)))
