import collections
import matplotlib.pyplot as plt
import numpy as np
from scipy import sparse
from scipy.sparse.linalg import spsolve
import pylab as plb
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
import matplotlib.pyplot as plt


def highOrderStrongGauss(posVector, sigmaVector):
    """
    Strong gauss provides a function to define a gauss, taking only the amplitude as parameter
    :param posVector: array of positions for the center
    :param sigmaVector: array of sigmas
    :return: function that defines a gauss sum
    """
    def fGauss(x, *params):
        y = np.zeros_like(x)
        for i in range(0, len(params)):
            ctr = float(posVector[i])
            amp = float(params[i])
            wid = float(sigmaVector[i])
            y = y + amp * np.exp( -((x - ctr)/wid)**2)
        return y
    return fGauss


def highOrderWeakGauss():
    def fGauss(x, *params):
        y = np.zeros_like(x)
        for i in range(0, len(params), 3):
            ctr = float(params[i])
            amp = float(params[i + 1])
            wid = float(params[i + 2])
            y = y + amp * np.exp( - ((x - ctr) / wid) ** 2)
        return y
    return fGauss


class GaussianFit:

    def __init__(self, xdata, ydata):
        self.xdata = np.array(xdata, dtype='float64')
        self.ydata = np.array(ydata, dtype='float64')

    def findGaussian(self, startPos, endPos, listPeaks, p0=None):
        resDict = {}
        peakValues = self.ydata[startPos:endPos]
        peakLocation = self.xdata[startPos:endPos]
        gaussF = highOrderWeakGauss()

        if p0 is None:
            p0=np.ones(3*np.size(listPeaks), dtype=np.float)
            i = 0
            for peak in listPeaks:
                p0[i * 3] = float(peak)
                p0[i * 3 + 1] = self.ydata[peak]
                p0[i * 3 + 2] = 3.0
                i += 1

        popt, pcov = curve_fit(gaussF, peakLocation, peakValues, p0=p0, maxfev=100000)

        for i in range(0, np.size(listPeaks)):
            resDict[listPeaks[i]]={}
            resDict[listPeaks[i]]['center'] = popt[3 * i]
            resDict[listPeaks[i]]['amplitude'] = popt[3 * i + 1]
            resDict[listPeaks[i]]['sigma'] = abs(popt[3 * i + 2])

        return resDict

    def fitGaussianList(self, widthDict, desiredPeaks):
        resDict = {}
        for peak in desiredPeaks:
            if widthDict[str(peak)] is not None:
                mixedPeaks = widthDict[str(peak)]['peaks']
                startPos = widthDict[str(peak)]['leftAdjacence']
                endPos = widthDict[str(peak)]['rightAdjacence']
                resDict[str(peak)] = self.findGaussian(startPos, endPos, mixedPeaks)
            else:
                resDict[str(peak)] = None

        return resDict

    def fitHardGaussian(self, startPos, endPos, mixedPeaks, listSigmas):
        resDict = {}
        peakValues = self.ydata[startPos:endPos]
        peakLocation = self.xdata[startPos:endPos]
        gauss = highOrderStrongGauss(mixedPeaks, listSigmas)

        popt, pcov = curve_fit(gauss, peakLocation, peakValues, p0=np.ones(np.size(mixedPeaks), dtype=np.float), maxfev=1000000)
        for i in range(0, np.size(mixedPeaks)):
            resDict[mixedPeaks[i]] = {}
            resDict[mixedPeaks[i]]['amplitude'] = popt[i]
            resDict[mixedPeaks[i]]['sigma'] = listSigmas[i]

        return resDict

    def fitGaussianListTips(self, widthDict, tipData):
        resDict = {}
        for peak in tipData.keys():
            if tipData[peak] is not None:
                mixedPeaks = widthDict[str(peak)]['peaks']
                startPos = widthDict[str(peak)]['leftAdjacence']
                endPos = widthDict[str(peak)]['rightAdjacence']
                sigmaList = []
                mixedPositionList = []
                for insidePeak in mixedPeaks:
                    aux1 = tipData[str(peak)]
                    aux2 = aux1[insidePeak]
                    sigmaList.append(aux2['sigma'])
                    mixedPositionList.append(aux2['center'])
                resDict[str(peak)] = self.fitHardGaussian(startPos, endPos, mixedPositionList, sigmaList)
            else:
                resDict[str(peak)] = None

        return resDict







#x = np.array(range(0,100))
#gauss = highOrderStrongGauss([30, 50, 60], [5, 10, 1])
#y = np.array(gauss(x, 50, 100, 30))
#noise = np.random.rand(100)*100
#ynoise = y + noise
#plt.plot(x, ynoise)
#guess = [np.array([1, 1, 1])]
#popt, pcov = curve_fit(gauss, x, ynoise, p0=guess)
#fit = gauss(x, *popt)
#plt.plot(x, fit)
#plt.show()

#gauss = highOrderStrongGauss([30, 50, 70], [10, 15, 1])
#gauss2 = highOrderWeakGauss([30, 50, 70])
#x = np.array(range(0,100))
#y = np.array(gauss(x, 200000, 250000, 30000))
#noise = np.random.rand(100)*1-0.5
#ynoise = y + noise


#g = GaussianFit(x, ynoise)
#res = g.findGaussian(20, 80, [30, 50, 70])
#print res
#plt.plot(x, ynoise)
#plt.plot(x, gauss2(x, res[30]['amplitude'], res[30]['sigma'], res[50]['amplitude'], res[50]['sigma'], res[70]['amplitude'], res[70]['sigma']))
#plt.show()