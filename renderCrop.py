from matplotlib import pyplot as plt
import numpy as np
from matplotlib.widgets import Button
import math
from matplotlib.patches import Ellipse

class InteractiveCrop:
    def __init__(self, H, S, eOrigin, eScale):
        #Define the code 0 exit flag
        self.exitFlag = False

        #Figure 1 creation
        self.fig = plt.figure('Crop Pixels')
        self.ax = self.fig.add_subplot(111)
        self.ax.set_title('Press and Drag to select the Crop area.\nPress and Drag twice define the crop square.')
        self.line, = self.ax.plot([0], [0], color='k', linewidth=3.0)  # empty line

        #Data creation for the first figure
        self.i = 0
        self.press = None
        self.H = H
        self.im = self.ax.imshow(self.H[:, :], interpolation='none')

        self.x1 = 5
        self.x2 = self.H.shape[1] - 6
        self.y1 = 5
        self.y2 = self.H.shape[0] - 6

        #Creation of the crop Square
        self.line.set_data([self.x1, self.x2, self.x2, self.x1, self.x1], [self.y1, self.y1, self.y2, self.y2, self.y1])

        #Creation of the circles
        self.circleRadius = max(self.H.shape[1], self.H.shape[0]) / 50
        self.circle1 = plt.Circle((self.x1, self.y1), self.circleRadius, color='r')
        self.circle2 = plt.Circle((self.x2, self.y2), self.circleRadius, color='g')
        self.ax.add_artist(self.circle1)
        self.ax.add_artist(self.circle2)

        #Creation of the coordinates text boxes
        self.textF1P1 = plt.figtext(0.05, 0.01, 'Point#1 > x = {}\n                 y = {}'.format(self.x1, self.y1))
        self.textF1P2 = plt.figtext(0.4, 0.01, 'Point#2 > x = {}\n                  y = {}'.format(self.x2, self.y2))

        #Creation of the continue button
        axcontinue = plt.axes([0.85, 0.1, 0.1, 0.065])
        self.bcontinue = Button(axcontinue, 'Finish')

        #Event Handling creation for figure 1
        self.cid1 = self.line.figure.canvas.mpl_connect('button_press_event', self.on_press)
        self.cid2 = self.line.figure.canvas.mpl_connect('button_release_event', self.on_release)
        self.cid3 = self.line.figure.canvas.mpl_connect('motion_notify_event', self.on_motion)
        self.bcontinue.on_clicked(self.on_continue)

        #Figure 2 creation
        self.fig2 = plt.figure('Crop Spectrum')
        self.ax2 = plt.subplot()
        plt.ylabel('counts')
        plt.xlabel('energy (eV)')

        self.max = int(np.max(S))
        self.min = int(np.min(S))

        #Creation of the data
        self.i2 = 0
        self.press2 = None
        self.S = S
        self.eOrigin = eOrigin
        self.eScale = eScale
        self.ex1 = eOrigin + 10
        self.ex2 = eOrigin + self.S.shape[0] * eScale - 10
        self.ey1 = self.max
        self.ey2 = self.min

        plt.subplots_adjust(bottom=0.2)
        horizontalScale = np.linspace(eOrigin, eOrigin + np.size(self.S, 0) * eScale, np.size(self.S, 0))
        self.l, = self.ax2.plot(horizontalScale, self.S[:], lw=2)
        self.ax2.set_title('Click and Drag on the Panel to select the Crop area.\nOne vertical line at a time.')
        self.line2, = self.ax2.plot([0], [0], color='#00E600', linewidth=3.0)
        self.line2.set_data([self.ex1, self.ex2, self.ex2, self.ex1, self.ex1], [self.ey1, self.ey1, self.ey2, self.ey2, self.ey1])
        self.line2.figure.canvas.draw()

        #Creation of the circles
        self.posEl1 = abs(self.ax2.get_ylim()[1] - self.ax2.get_ylim()[0]) / 2
        self.ells1 = Ellipse(xy=(self.ex1, self.posEl1), width=abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30, height=abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30 )
        self.ells2 = Ellipse(xy=(self.ex2, self.posEl1), width=abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30, height=abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30 )
        self.ax2.add_artist(self.ells1)
        self.ells1.set_facecolor('r')
        self.ax2.add_artist(self.ells2)
        self.ells2.set_facecolor('k')

        #Creation of the coordinates text boxes
        self.textF2P1 = plt.figtext(0.05, 0.01, 'Point#1 > x = {}'.format(self.ex1))
        self.textF2P2 = plt.figtext(0.4, 0.01, 'Point#2 > x = {}'.format(self.ex2))

        #Event Handling for figure 2
        self.sid1 = self.line2.figure.canvas.mpl_connect('button_press_event', self.on_press2)
        self.sid2 = self.line2.figure.canvas.mpl_connect('button_release_event', self.on_release2)
        self.sid3 = self.line2.figure.canvas.mpl_connect('motion_notify_event', self.on_motion2)

        plt.show()

    def on_press(self, event):
        if event.inaxes != self.line.axes: return

        if math.pow(event.xdata - self.x1, 2) + math.pow(event.ydata - self.y1, 2) < math.pow(self.circleRadius, 2):
            self.press = 1
            self.i = 1
        elif math.pow(event.xdata - self.x2,2) + math.pow(event.ydata - self.y2, 2) < math.pow(self.circleRadius, 2):
            self.press = 1
            self.i = 2

        #if self.i % 2 == 1:
        #    self.ax.set_title('Now Drag to change the first point\'s location.\nRelease when the done.')
        #else:
        #    self.ax.set_title('Now Drag to change the second point\'s location.\nRelease when the done.')
        self.line.figure.canvas.draw()

    def on_motion(self, event):
        if self.press is None: return
        if event.inaxes != self.line.axes: return
        if self.i % 2 == 1:
            self.x1 = max(min(int(event.xdata), self.H.shape[1] - 1), 0)
            self.y1 = max(min(int(event.ydata), self.H.shape[0] - 1), 0)
        else:
            self.x2 = max(min(int(event.xdata), self.H.shape[1] - 1), 0)
            self.y2 = max(min(int(event.ydata), self.H.shape[0] - 1), 0)

        self.textF1P1.set_text('Point#1 > x = {}\n                 y = {}'.format(self.x1, self.y1))
        self.textF1P2.set_text('Point#2 > x = {}\n                  y = {}'.format(self.x2, self.y2))

        #apagar
        self.circle1.remove()
        self.circle2.remove()
        self.circle1 = plt.Circle((self.x1, self.y1), self.circleRadius, color='r')
        self.circle2 = plt.Circle((self.x2, self.y2), self.circleRadius, color='g')
        self.ax.add_artist(self.circle1)
        self.ax.add_artist(self.circle2)

        self.line.set_data([self.x1, self.x2, self.x2, self.x1, self.x1], [self.y1, self.y1, self.y2, self.y2, self.y1])
        self.line.figure.canvas.draw()

    def on_release(self, event):
        #if event.inaxes!=self.line.axes: return
        self.press = None

        #if self.i % 2 == 1:
        #    self.ax.set_title('Press and Drag to change the second point\'s location.\nAlternativelly press Continue to finish.')
        #else:
        #    self.ax.set_title('Press and Drag to change the first point\'s location.\nAlternativelly press Continue to finish.')
        self.line.figure.canvas.draw()

    @staticmethod
    def inside_elipse(x, y, h, k, rx, ry):
        return math.pow(x - h, 2) / float(math.pow(rx, 2)) + math.pow(y - k, 2) / float(math.pow(ry, 2)) < 1

    #Second figure
    def on_press2(self, event):
        if event.inaxes != self.line2.axes: return

        auxRX = abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30
        auxRY = abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30
        if self.inside_elipse(event.xdata, event.ydata, self.ex1, self.posEl1, auxRX, auxRY):
            self.press2 = 1
            self.i2 = 1
        if self.inside_elipse(event.xdata, event.ydata, self.ex2, self.posEl1, auxRX, auxRY):
            self.press2 = 1
            self.i2 = 2

        self.line2.figure.canvas.draw()
        # self.press2 = 1
        # self.i2 += 1
        #
        # if self.i2 % 2 == 1:
        #     self.ex1 = max(min(int(event.xdata), self.eOrigin + np.size(self.S, 0) * self.eScale - 1), self.eOrigin)
        # else:
        #     self.ex2 = max(min(int(event.xdata), self.eOrigin + np.size(self.S, 0) * self.eScale - 1), self.eOrigin)
        #
        # self.textF2P1.set_text('Point#1 > x = {}'.format(self.ex1))
        # self.textF2P2.set_text('Point#2 > x = {}'.format(self.ex2))
        #
        # self.line2.set_data([self.ex1, self.ex2, self.ex2, self.ex1, self.ex1], [self.ey1, self.ey1, self.ey2, self.ey2, self.ey1])
        #
        # if self.i2 % 2 == 1:
        #     self.ax2.set_title('Now Drag to change the first point\'s location.\nRelease when the done.')
        # else:
        #     self.ax2.set_title('Now Drag to change the second point\'s location.\nRelease when the done.')
        # self.line2.figure.canvas.draw()

    def on_motion2(self, event):
        if self.press2 is None:
            upperLimit = self.ax2.get_ylim()[1] - abs(self.ax2.get_ylim()[1] - self.ax2.get_ylim()[0]) / 20
            lowerLimit = self.ax2.get_ylim()[0] + abs(self.ax2.get_ylim()[1] - self.ax2.get_ylim()[0]) / 20
            if self.posEl1 > upperLimit:
                self.posEl1 = max(upperLimit, self.ey2)
            if self.posEl1 < lowerLimit:
                self.posEl1 = min(lowerLimit, self.ey1)
            #opcionais, mudar as coordenadas x dos pontos quando e zoom muda
            #if self.ex1 < self.ax2.get_xlim()[0]:
            #    self.ex1 = self.ax2.get_xlim()[0]
            #if self.ex2 < self.ax2.get_xlim()[1]:
            #    self.ex2 = self.ax2.get_xlim()[1]
            self.ells1.remove()
            self.ells2.remove()
            self.ells1 = Ellipse(xy=(self.ex1, self.posEl1), width=abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30, height=abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30 )
            self.ells2 = Ellipse(xy=(self.ex2, self.posEl1), width=abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30, height=abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30 )
            self.ax2.add_artist(self.ells1)
            self.ells1.set_facecolor('r')
            self.ax2.add_artist(self.ells2)
            self.ells2.set_facecolor('k')
            return
        if event.inaxes != self.line2.axes:
            return
        if self.i2 % 2 == 1:
            self.ex1 = max(min(int(event.xdata), self.eOrigin + np.size(self.S, 0) * self.eScale - 1), self.eOrigin)
            self.posEl1 = max(min(event.ydata, self.max), self.min)
            self.ells1.remove()
            self.ells2.remove()
            self.ells1 = Ellipse(xy=(self.ex1, self.posEl1), width=abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30, height=abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30 )
            self.ells2 = Ellipse(xy=(self.ex2, self.posEl1), width=abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30, height=abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30 )
            self.ax2.add_artist(self.ells1)
            self.ells1.set_facecolor('r')
            self.ax2.add_artist(self.ells2)
            self.ells2.set_facecolor('k')
        else:
            self.ex2 = max(min(int(event.xdata), self.eOrigin + np.size(self.S, 0) * self.eScale - 1), self.eOrigin)
            self.posEl1 = max(min(event.ydata, self.max), self.min)
            self.ells1.remove()
            self.ells2.remove()
            self.ells1 = Ellipse(xy=(self.ex1, self.posEl1), width=abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30, height=abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30 )
            self.ells2 = Ellipse(xy=(self.ex2, self.posEl1), width=abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30, height=abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30 )
            self.ax2.add_artist(self.ells1)
            self.ells1.set_facecolor('r')
            self.ax2.add_artist(self.ells2)
            self.ells2.set_facecolor('k')

        self.textF2P1.set_text('Point#1 > x = {}'.format(self.ex1))
        self.textF2P2.set_text('Point#2 > x = {}'.format(self.ex2))

        self.line2.set_data([self.ex1, self.ex2, self.ex2, self.ex1, self.ex1], [self.ey1, self.ey1, self.ey2, self.ey2, self.ey1])
        self.line2.figure.canvas.draw()

    def on_release2(self, event):
        self.press2 = None

        self.line2.figure.canvas.draw()

    def on_continue(self, event):
        if self.x1 > self.x2:
            aux = self.x1
            self.x1 = self.x2
            self.x2 = aux
        if self.y1 > self.y2:
            aux = self.y1
            self.y1 = self.y2
            self.y2 = aux
        if self.ex1 > self.ex2:
            aux = self.ex1
            self.ex1 = self.ex2
            self.ex2 = aux

        self.exitFlag = True
        plt.figure('Crop Pixels')
        plt.close()
        plt.figure('Crop Spectrum')
        plt.close()
