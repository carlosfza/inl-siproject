import numpy as np
def absoluteTrapz(yData, xData=None, dx=1):
    yDataPositive = map((lambda x: 0 if x < 0 else x), yData)
    yDataNegative = map((lambda x: -x if x < 0 else 0), yData)
    return np.trapz(yDataPositive, xData, dx) + np.trapz(yDataNegative, xData, dx)




