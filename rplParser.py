import re
import numpy as np


class RplParser:

	def __init__(self, filepath):
		rplKeys = {}
		frpl = open( filepath + '.rpl', 'r')

		for line in frpl:
			kv = re.split('[ \t\n\r\f\v]+', line)
			rplKeys[kv[0]] = kv[1]

		#Verification wether the tags give us enought information, might need some clean up
		valid = True
		if 'data-Length' in rplKeys:
			data_length = rplKeys['data-Length']
		else:
			data_length = ''
			valid = False
		if 'data-type'in rplKeys:
			data_type = rplKeys['data-type']
		else:
			data_type = ''
			valid = False
		if 'width' in rplKeys and 'height' in rplKeys and 'depth' in rplKeys:
			self.mWidth = int(rplKeys['width'])
			self.mHeight = int(rplKeys['height'])
			self.energyLevels = int(rplKeys['depth'])
		else:
			valid = False

		#Dict with the possible type options, update the options in case another new type comes
		dataTypeOptions = dict(float={'2': np.float16, '4': np.float32, '8': np.float64}, unsigned={'1': np.uint8, '2': np.uint16, '4': np.uint32, '8': np.uint64})

		#Verify if the type given is a valid type
		if data_type in dataTypeOptions and data_length in dataTypeOptions[data_type]:
			pass
		else:
			valid = False

		#Read data from the .raw and put it into a numpy arrray
		if valid:
			self.data = np.fromfile(filepath + '.raw', dataTypeOptions[data_type][data_length])
			self.data = np.reshape(self.data, (int(rplKeys['height']), int(rplKeys['width']), int(rplKeys['depth']) ))

		frpl.close()

	def dumpData(self, filepath):
		print len(self.data)
		print len(self.data[0])
		print len(self.data[0][0])

		fw = open(filepath, 'w')
		for line in self.data:
			for pixel in line:
				for value in pixel:
					fw.write(str(value))
					fw.write(' ')
				fw.write('\n')
		fw.close()
