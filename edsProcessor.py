import numpy as np
from sympy.functions.elementary.integers import ceiling

import noiseCalculator
import baselineFit
import peakDetection
import cubeData
from gaussianFit import GaussianFit


class EDSProcessor:
    """
    EDS Processor extracts information of an EDS dataCube
    """

    def __init__(self, dataCube):
        self.dataCube = dataCube
        self.p1 = -1
        self.p2 = -2

    def definePeaklessZone(self, p1, p2):
        """Define the peakless zone separately
        :param p1: start of the peakless zone
        :param p2: end of the peakless zone
        """
        self.p1 = p1
        self.p2 = p2

    def _getNoiseArray(self, h1, w1, h2, w2, fR1, fR2, sR1, sR2, reduced, method='linear'):
        yData1 = self.getSubSpectrum(h1, w1, h2, w2, fR1, fR2, reduced)
        yData2 = self.getSubSpectrum(h1, w1, h2, w2, sR1, sR2, reduced)
        signal = self.getSubSpectrum(h1, w1, h2, w2, self.dataCube.eOrigin, self.dataCube.eOrigin + self.dataCube.energyLevels * self.dataCube.eScale, reduced)

        noiseC = noiseCalculator.NoiseCalculator(yData1, fR1, yData2, sR1)
        res = noiseC.calculateArrayNoise(signal, method, scaling='energy')
        return res

    def getSubSpectrum(self, h1, w1, h2, w2, e1, e2, reduced=False):
        #
        #Retrieves a subSpectrum from the dataCube, performing a sum for all points between (h1,w1) and (h2,w2) in the energy region e1-e2 for the reduced or regular data cube
        #
        if reduced:
            yData = self.dataCube.reducedCube[h1:h2, w1:w2, self.dataCube.getEnergyPos(e1):self.dataCube.getEnergyPos(e2)]
        else:
            yData = self.dataCube.pixelMatrix[h1:h2, w1:w2, self.dataCube.getEnergyPos(e1):self.dataCube.getEnergyPos(e2)]
        yData = np.sum(yData, 0)
        yData = np.sum(yData, 0)
        return yData

    def _separateBaseLine(self, h1, w1, h2, w2, e1, e2, reduced=False):
        """
        Separates baseline from a Spectrum, returns the spectrum minus the baseline.
        """
        yData = self.getSubSpectrum(h1, w1, h2, w2, e1, e2, reduced)

        bsf = baselineFit.BaselineFit()
        baseline = bsf.baseline_als(yData, 100000, 0.0005, 100)

        return yData - baseline

    @staticmethod
    def _detectPeaks(yData, noise):
        """
        Calls the peak detection on the data
        :param yData: EDS Spectrum, numpy Array with y values
        :param noise: Noise Array
        :return: local maximum and minimum peak list
        """
        maxTab, minTab = peakDetection.peakdetect(yData, range(0, np.size(yData)), 5, noise)
        return maxTab, minTab

    def processEDS(self, h1, w1, h2, w2, e1, e2, fn1, fn2, sn1, sn2, reduced=False):
        noise = self._getNoiseArray(h1, w1, h2, w2, fn1, fn2, sn1, sn2, reduced, method='linear')
        #In the EDS Model, if there is no noise we will just asume that there are not enought counts in order to make a peak separation meaningful
        #print abs(np.sum(noise) - 0)
        if abs(np.sum(noise) - 0) <= 0.001:
            print 'There were no Counts in the noise area.'
            return [], [], [], None
        else:
            yData = self._separateBaseLine(h1, w1, h2, w2, e1, e2, reduced)
            maxt, mint = self._detectPeaks(yData, 4 * noise)
        return maxt, mint, yData, noise

    def trackAtomsEDS(self, nR1, nR2, sR1, sR2, maxReduceSize, peakArray, peakDivergence, reductionMethod=cubeData.DataCube.reduceCube):
        res = {}
        res['reductions'] = {}
        for peak in peakArray:
            res[str(peak)] = {}

        for r in xrange(0, maxReduceSize + 1):
            print 'Inicio da reducao de valor {}'.format(pow(2, r))
            reductionMethod(self.dataCube, pow(2, r))
            res['reductions'][r] = {}
            res['reductions'][r]['reducedAggregateHeight'] = self.dataCube.reducedAggregateHeight
            res['reductions'][r]['reducedAggregateWidth'] = self.dataCube.reducedAggregateWidth
            res['reductions'][r]['reducedMissingHeight'] = self.dataCube.reducedMissingHeight
            res['reductions'][r]['reducedMissingWidth'] = self.dataCube.reducedMissingWidth
            res['reductions'][r]['reducedPixelHeightSize'] = self.dataCube.reducedAggregateHeight * self.dataCube.pixelHeightSize
            res['reductions'][r]['reducedPixelWidthSize'] = self.dataCube.reducedAggregateWidth * self.dataCube.pixelWidthSize
            for peak in peakArray:
                res[str(peak)][r] = np.zeros((self.dataCube.reducedHeight, self.dataCube.reducedWidth))
            for h in xrange(0, self.dataCube.reducedHeight):
                for w in xrange(0, self.dataCube.reducedWidth):
                    print 'Inicio do pixel {} - {}'.format(h, w)
                    maxt, mint, _, noiseArray = self.processEDS(h, w, h + 1, w + 1, self.dataCube.eOrigin, self.dataCube.energyLevels * self.dataCube.eScale + self.dataCube.eOrigin, nR1, nR2, sR1, sR2, True)
                    if noiseArray is None:
                        for peak in peakArray:
                            res[str(peak)][r][h, w] = -1
                    else:
                        for peak in peakArray:
                            peakDetected = self.returnPeakIntensity(maxt, int(peak - self.dataCube.eOrigin) / self.dataCube.eScale, peakDivergence)
                            if peakDetected is not None:
                                res[str(peak)][r][h, w] = peakDetected[1]
                            else:
                                res[str(peak)][r][h, w] = 0
        return res

    def atomsProportionsEDS(self, nR1, nR2, sR1, sR2, maxReduceSize, peakArray, peakDivergence, reductionMethod=cubeData.DataCube.reduceCube):
        res = {}
        res['reductions'] = {}
        for peak in peakArray:
            res[str(peak)] = {}
            res['{} - Noise'.format(peak)] = {}

        for r in xrange(0, maxReduceSize + 1):
            print 'Inicio da reducao de valor {}'.format(pow(2, r))
            reductionMethod(self.dataCube, pow(2, r))
            res['reductions'][r] = {}
            res['reductions'][r]['reducedAggregateHeight'] = self.dataCube.reducedAggregateHeight
            res['reductions'][r]['reducedAggregateWidth'] = self.dataCube.reducedAggregateWidth
            res['reductions'][r]['reducedMissingHeight'] = self.dataCube.reducedMissingHeight
            res['reductions'][r]['reducedMissingWidth'] = self.dataCube.reducedMissingWidth
            res['reductions'][r]['reducedPixelHeightSize'] = self.dataCube.reducedAggregateHeight * self.dataCube.pixelHeightSize
            res['reductions'][r]['reducedPixelWidthSize'] = self.dataCube.reducedAggregateWidth * self.dataCube.pixelWidthSize
            for peak in peakArray:
                res[str(peak)][r] = np.zeros((self.dataCube.reducedHeight, self.dataCube.reducedWidth))
                res['{} - Noise'.format(peak)][r] = np.zeros((self.dataCube.reducedHeight, self.dataCube.reducedWidth))
            for h in xrange(0, self.dataCube.reducedHeight):
                for w in xrange(0, self.dataCube.reducedWidth):
                    print 'Inicio do pixel {} - {}'.format(h, w)
                    maxt, mint, yDataReduced, noiseArray = self.processEDS(h, w, h + 1, w + 1, self.dataCube.eOrigin, self.dataCube.energyLevels * self.dataCube.eScale + self.dataCube.eOrigin, nR1, nR2, sR1, sR2, True)
                    if noiseArray is None:
                        for peak in peakArray:
                            res[str(peak)][r][h, w] = -1
                            res['{} - Noise'.format(peak)][r][h, w] = 0
                    else:
                        widthData = self.fullWidthAtHalfMaximum(maxt, mint, yDataReduced)
                        integrations = self.integratePeaks(maxt, widthData, yDataReduced, dx=self.dataCube.eScale)
                        for peak in peakArray:
                            peakDetectedLocation = self.returnPeakLocation(integrations, int(peak - self.dataCube.eOrigin) / self.dataCube.eScale, peakDivergence)
                            if peakDetectedLocation is not None:
                                res[str(peak)][r][h, w] = integrations[peakDetectedLocation][1]
                                res['{} - Noise'.format(peak)][r][h, w] = np.trapz(2 * noiseArray[widthData[peakDetectedLocation, 0] : widthData[peakDetectedLocation, 1]], dx=self.dataCube.eScale)
                            else:
                                res[str(peak)][r][h, w] = 0
                                res['{} - Noise'.format(peak)][r][h, w] = 0
        return res

    def atomsProportionsEDSV2(self, nR1, nR2, sR1, sR2, maxReduceSize, peakArray, peakDivergence, reductionMethod=cubeData.DataCube.reduceCube):
        maxt = None
        res = {}
        res['reductions'] = {}
        for peak in peakArray:
            res[str(peak)] = {}
            res['{} - Noise'.format(peak)] = {}

        for r in xrange(0, maxReduceSize + 1):
            print 'Inicio da reducao de valor {}'.format(pow(2, r))
            reductionMethod(self.dataCube, pow(2, r))
            res['reductions'][r] = {}
            res['reductions'][r]['reducedAggregateHeight'] = self.dataCube.reducedAggregateHeight
            res['reductions'][r]['reducedAggregateWidth'] = self.dataCube.reducedAggregateWidth
            res['reductions'][r]['reducedMissingHeight'] = self.dataCube.reducedMissingHeight
            res['reductions'][r]['reducedMissingWidth'] = self.dataCube.reducedMissingWidth
            res['reductions'][r]['reducedPixelHeightSize'] = self.dataCube.reducedAggregateHeight * self.dataCube.pixelHeightSize
            res['reductions'][r]['reducedPixelWidthSize'] = self.dataCube.reducedAggregateWidth * self.dataCube.pixelWidthSize
            for peak in peakArray:
                res[str(peak)][r] = np.zeros((self.dataCube.reducedHeight, self.dataCube.reducedWidth))
                res['{} - Noise'.format(peak)][r] = np.zeros((self.dataCube.reducedHeight, self.dataCube.reducedWidth))
            for h in xrange(0, self.dataCube.reducedHeight):
                for w in xrange(0, self.dataCube.reducedWidth):
                    print 'Inicio do pixel {} - {}'.format(h, w)
                    if r == 0:
                        maxt, mint, yDataReduced, noiseArray = self.processEDS(h, w, h + 1, w + 1, self.dataCube.eOrigin, self.dataCube.energyLevels * self.dataCube.eScale + self.dataCube.eOrigin, nR1, nR2, sR1, sR2, True)
                        if noiseArray is None:
                            for peak in peakArray:
                                res[str(peak)][r][h, w] = -1
                                res['{} - Noise'.format(peak)][r][h, w] = 0
                        else:
                            widthData = self.fullWidthAtHalfMaximum(maxt, mint, yDataReduced)
                            integrations = self.integratePeaks(maxt, widthData, yDataReduced, dx=self.dataCube.eScale)
                            for peak in peakArray:
                                peakDetectedLocation = self.returnPeakLocation(integrations, int(peak - self.dataCube.eOrigin) / self.dataCube.eScale, peakDivergence)
                                if peakDetectedLocation is not None:
                                    res[str(peak)][r][h, w] = integrations[peakDetectedLocation][1]
                                    res['{} - Noise'.format(peak)][r][h, w] = np.trapz(2 * noiseArray[widthData[peakDetectedLocation, 0] : widthData[peakDetectedLocation, 1]], dx=self.dataCube.eScale)
                                else:
                                    res[str(peak)][r][h, w] = 0
                                    res['{} - Noise'.format(peak)][r][h, w] = 0

                    else:
                        yDataReduced = self._separateBaseLine(h, w, h + 1, w + 1, self.dataCube.eOrigin, self.dataCube.energyLevels * self.dataCube.eScale + self.dataCube.eOrigin, True)
                        noiseArray = self._getNoiseArray(h, w, h + 1, w + 1, nR1, nR2, sR1, sR2, True, method='linear')
                        if abs(np.sum(noiseArray) - 0) <= 0.001:
                            for peak in peakArray:
                                res[str(peak)][r][h, w] = -1
                                res['{} - Noise'.format(peak)][r][h, w] = 0
                        else:
                            integrations = self.integratePeaks(maxt, widthData, yDataReduced, dx=self.dataCube.eScale)
                            for peak in peakArray:
                                peakDetectedLocation = self.returnPeakLocation(integrations, int(peak - self.dataCube.eOrigin) / self.dataCube.eScale, peakDivergence)
                                if peakDetectedLocation is not None:
                                    res[str(peak)][r][h, w] = integrations[peakDetectedLocation][1]
                                    res['{} - Noise'.format(peak)][r][h, w] = np.trapz(2 * noiseArray[widthData[peakDetectedLocation, 0] : widthData[peakDetectedLocation, 1]], dx=self.dataCube.eScale)
                                else:
                                    res[str(peak)][r][h, w] = -1
                                    res['{} - Noise'.format(peak)][r][h, w] = -1
        return res

    def atomsProportionsEDSGauss(self, nR1, nR2, sR1, sR2, maxReduceSize, peakArray, peakDivergence, reductionMethod=cubeData.DataCube.reduceCube):
        maxt = None
        peakArrayLocation = np.array((np.array(peakArray) - self.dataCube.eOrigin) / self.dataCube.eScale, dtype=int)
        res = {}
        res['reductions'] = {}
        for peak in peakArray:
            res[str(peak)] = {}
            res['{} - Noise'.format(peak)] = {}

        for r in xrange(0, maxReduceSize + 1):
            print 'Inicio da reducao de valor {}'.format(pow(2, r))
            reductionMethod(self.dataCube, pow(2, r))
            res['reductions'][r] = {}
            res['reductions'][r]['reducedAggregateHeight'] = self.dataCube.reducedAggregateHeight
            res['reductions'][r]['reducedAggregateWidth'] = self.dataCube.reducedAggregateWidth
            res['reductions'][r]['reducedMissingHeight'] = self.dataCube.reducedMissingHeight
            res['reductions'][r]['reducedMissingWidth'] = self.dataCube.reducedMissingWidth
            res['reductions'][r]['reducedPixelHeightSize'] = self.dataCube.reducedAggregateHeight * self.dataCube.pixelHeightSize
            res['reductions'][r]['reducedPixelWidthSize'] = self.dataCube.reducedAggregateWidth * self.dataCube.pixelWidthSize
            for peak in peakArray:
                res[str(peak)][r] = np.zeros((self.dataCube.reducedHeight, self.dataCube.reducedWidth))
                res['{} - Noise'.format(peak)][r] = np.zeros((self.dataCube.reducedHeight, self.dataCube.reducedWidth))
            for h in xrange(0, self.dataCube.reducedHeight):
                for w in xrange(0, self.dataCube.reducedWidth):
                    print 'Inicio do pixel {} - {}'.format(h, w)
                    if r == 0:
                        maxt, mint, yDataReduced, noiseArray = self.processEDS(h, w, h + 1, w + 1, self.dataCube.eOrigin, self.dataCube.energyLevels * self.dataCube.eScale + self.dataCube.eOrigin, nR1, nR2, sR1, sR2, True)
                        if noiseArray is None:
                            for peak in peakArray:
                                res[str(peak)][r][h, w] = -1
                                res['{} - Noise'.format(peak)][r][h, w] = 0
                                raise Exception('You probably defined badly the noise positions, scale or something like that.')
                        else:
                            widthData = self.fullWidthAtHalfMaximum(maxt, mint, yDataReduced)
                            peakWidths = self.getRelevantPeakAreas(peakArrayLocation, maxt, widthData, peakDivergence)
                            gaussFit = GaussianFit(np.linspace(0, self.dataCube.energyLevels -1, self.dataCube.energyLevels), yDataReduced)
                            gaussIterationOne = gaussFit.fitGaussianList(peakWidths, np.array((np.array(peakArray) - self.dataCube.eOrigin) / self.dataCube.eScale, dtype=int))
                            gaussDict = gaussFit.fitGaussianListTips(peakWidths, gaussIterationOne)
                            gaussArea = self.getGaussianArea(peakWidths, gaussDict, maxt, mint)
                            integration_gauss = self.integrateGaussians(gaussArea, gaussDict, yDataReduced)
                            print gaussIterationOne

                            for peak in peakArray:
                                positionInArray = str(peakArrayLocation[peakArray.index(peak)])
                                if positionInArray in integration_gauss.keys():
                                    res[str(peak)][r][h, w] = integration_gauss[positionInArray]['intensity']
                                    res['{} - Noise'.format(peak)][r][h, w] = integration_gauss[positionInArray]['noise']
                                else:
                                    res[str(peak)][r][h, w] = -1
                                    res['{} - Noise'.format(peak)][r][h, w] = -1

                    else:
                        yDataReduced = self._separateBaseLine(h, w, h + 1, w + 1, self.dataCube.eOrigin, self.dataCube.energyLevels * self.dataCube.eScale + self.dataCube.eOrigin, True)

                        gaussFit = GaussianFit(np.linspace(0, self.dataCube.energyLevels - 1, self.dataCube.energyLevels), yDataReduced)
                        gaussDict = gaussFit.fitGaussianListTips(peakWidths, gaussIterationOne)
                        gaussArea = self.getGaussianArea(peakWidths, gaussDict, maxt, mint)
                        integration_gauss = self.integrateGaussians(gaussArea, gaussDict, yDataReduced)
                        print gaussDict

                        for peak in peakArray:
                            positionInArray = str(peakArrayLocation[peakArray.index(peak)])
                            if positionInArray in integration_gauss.keys():
                                res[str(peak)][r][h, w] = integration_gauss[positionInArray]['intensity']
                                res['{} - Noise'.format(peak)][r][h, w] = integration_gauss[positionInArray]['noise']
                            else:
                                res[str(peak)][r][h, w] = -1
                                res['{} - Noise'.format(peak)][r][h, w] = -1
        return res

    @staticmethod
    def returnPeakIntensity(maxT, position, peakDivergence):
        res = None
        for pair in maxT:
            #See if there was a peak in the tolerance area
            if abs(pair[0] - position) < peakDivergence:
                #If there are multiple peaks, which may happen in a some very fringe cases, get only the highest itensity one
                if res is None or pair[1] > res[1]:
                    res = pair
        return res

    @staticmethod
    def returnPeakLocation(maxT, position, peakDivergence):
        res = None
        resPosition = None
        for count in xrange(0, np.size(maxT, 0)):
            #See if there was a peak in the tolerance area
            if abs(maxT[count][0] - position) < peakDivergence:
                #If there are multiple peaks, which may happen in a some very fringe cases, get only the highest itensity one
                if res is None or maxT[count][1] > maxT[resPosition][1]:
                    res = maxT[count]
                    resPosition = count
        return resPosition

    @staticmethod
    def fullWidthAtHalfMaximum(maxT, minT, dataY, Fn=0.6, Hn=0.5, simetry=True): #mudar o Hn para 0.2 para testes
        res = np.zeros((np.size(maxT, 0), 2), dtype=np.int)
        #it is required that there are two minimums surrounding each maximum
        #the first step is to add minimuns to the begining and the end if there are none
        if len(maxT) == 0 or len(minT) == 0:
            return
        if maxT[0][0] < minT[0][0]:
            aux = [[0, dataY[0]]]
            minT = aux + minT
        if maxT[np.size(maxT, 0) - 1][0] > minT[np.size(minT, 0) - 1][0]:
            aux = [[int(np.size(dataY, 0)) - 1, dataY[np.size(dataY, 0) - 1]]]
            minT = minT + aux

        #For every minimum, except the last one, get the next maximum
        #then before the peak, but after the prevMin, get the first value of intensity greater then Hn * intensity of the peak,
        #finally after the peak, but before the postMin, get the first value of intensity lesse than Hn * intensity of the peak
        for cont in xrange(0, np.size(minT, 0) - 1):
            widthPeak = np.zeros(2)
            widthPeak[0] = widthPeak[1] = -1
            ltMin = minT[cont]
            gtMin = minT[cont + 1]
            peak = maxT[cont]

            for position in xrange(ltMin[0], peak[0]):
                if dataY[position + 1] - min(ltMin[1], gtMin[1]) > Hn * (peak[1] - min(ltMin[1], gtMin[1])) and dataY[position] - min(ltMin[1], gtMin[1]) <= Hn * (peak[1] - min(ltMin[1], gtMin[1])):
                    widthPeak[0] = position
                    #break
            for position in xrange(peak[0], gtMin[0] + 1):
                if dataY[position] - min(ltMin[1], gtMin[1]) < Hn * (peak[1] - min(ltMin[1], gtMin[1])):
                    widthPeak[1] = position
                    break
            if widthPeak[0] == -1:
                widthPeak[0] = ltMin[0]
            if widthPeak[1] == -1:
                widthPeak[1] = gtMin[0]
            #Perfor the dilatation, using Fn
            extmin = ((widthPeak[0] - peak[0]) * (1.0 / Fn))
            extmax = ((widthPeak[1] - peak[0]) * (1.0 / Fn))
            widthPeak[0] = max(peak[0] + ((widthPeak[0] - peak[0]) * (1.0 / Fn)), ltMin[0]) #max(widthPeak[0] + min(-3, int((widthPeak[0] - peak[0])) * (1.0 / Fn)), ltMin[0])
            widthPeak[1] = min(peak[0] + ((widthPeak[1] - peak[0]) * (1.0 / Fn)), gtMin[0])#min(widthPeak[1] + min(3, int((widthPeak[1] - peak[0])) * (1.0 / Fn)), gtMin[0])
            if simetry:
                maxdistance = max(peak[0] - widthPeak[0], widthPeak[1] - peak[0])
                widthPeak[0] = max(peak[0] - maxdistance, ltMin[0])
                widthPeak[1] = min(peak[0] + maxdistance, gtMin[0])
            if widthPeak[0] != -1 and widthPeak[1] != -1:
                res[cont, :] = widthPeak

        return res

    @staticmethod
    def integratePeaks(peaks, peakWidths, ydata, xdata=None, dx=None):
        res = np.zeros((np.size(peakWidths, 0), 2))

        if xdata is not None:
            for count in xrange(0, np.size(peakWidths, 0)):
                res[count, 0] = peaks[count][0]
                res[count, 1] = np.trapz(ydata[peakWidths[count, 0] : peakWidths[count, 1]], x=xdata[peakWidths[count, 0] : peakWidths[count, 1]])
        elif dx is not None:
            for count in xrange(0, np.size(peakWidths, 0)):
                res[count, 0] = peaks[count][0]
                res[count, 1] = np.trapz(ydata[peakWidths[count, 0] : peakWidths[count, 1]], dx=dx)
        else:
            for count in xrange(0, np.size(peakWidths, 0)):
                res[count, 0] = peaks[count][0]
                res[count, 1] = np.trapz(ydata[peakWidths[count, 0] : peakWidths[count, 1]])

        return res

    def getRelevantPeakAreas(self, peakList, maxT, widthData, peakDivergence):
        """
        So here is how this works, we have a peak list that we want to monitor, and width information on each of these peaks. We now want one thing:
        To find which peaks are independent and which are not. This is not a very easy task, however, because we previously used FullWidthAtHalfMaximum
        it is actually trivial..ish.
        The proposed algorithm is the following, first we find which relevant peaks were found, then find the peaks related to those, then we package this information
        into the result

        :param self:
        :param peakList: List of the desired peaks we wish to monitor, actually an npArray
        :param maxT: Peaks found by the EDSProcessor
        :param widthData: widthInformation of each and every peak found previously
        :param peakDivergence: max divergence of the expected peak
        :return: A dict containing each relevant peak, it's related peaks, and the area in which the related peaks are present
        """
        res = {}
        for peak in peakList:
            peakDetectedLocation = self.returnPeakLocation(maxT, peak, peakDivergence)
            if peakDetectedLocation is not None:
                res[str(peak)] = {}
                res[str(peak)]['peaks'] = []
                res[str(peak)]['peaks'].append(maxT[peakDetectedLocation][0])

                #Find peaks adjacent to the selected peak
                res[str(peak)]['leftAdjacence'] = widthData[peakDetectedLocation, 0]
                leftAdjacence = peakDetectedLocation - 1
                while 0 < leftAdjacence < np.size(maxT, 0) and widthData[leftAdjacence + 1, 0] == widthData[leftAdjacence, 1]:
                    res[str(peak)]['peaks'].append(self.getPeakInTheArea(widthData, maxT, leftAdjacence))
                    res[str(peak)]['leftAdjacence'] = widthData[leftAdjacence, 0]
                    leftAdjacence -= 1

                res[str(peak)]['rightAdjacence'] = widthData[peakDetectedLocation, 1]
                rightAdjacence = peakDetectedLocation + 1
                while 0 < rightAdjacence < np.size(maxT, 0) and widthData[rightAdjacence - 1, 1] == widthData[rightAdjacence, 0]:
                    res[str(peak)]['peaks'].append(self.getPeakInTheArea(widthData, maxT, rightAdjacence))
                    res[str(peak)]['rightAdjacence'] = widthData[rightAdjacence, 1]
                    rightAdjacence += 1
            else:
                res[str(peak)] = None

        return res

    def getPeakInTheArea(self, widthData, maxT, position):
        minP = widthData[position, 0]
        maxP = widthData[position, 1]
        for peak in maxT:
            if minP <= peak[0]:
                if peak[0] <= maxP:
                    return peak[0]
        return -1

    def getGaussianArea(self, peakWidths, gaussDict, maxT, minT):
        #This functions calculates the relevant area to use when calculating the intensity and the error of the atom proportions gaussian
        #result format is {'#peak': {'position': '#position', 'distance': #distance}, ...}
        result = {}
        for peak, value in gaussDict.iteritems():
            mixedPeaks = value.keys()
            peakLocation = self.closestNumber(int(peak), mixedPeaks)
            minSigma = peakLocation - int(2 * value[peakLocation]['sigma'])
            maxSigma = peakLocation + int(2 * value[peakLocation]['sigma'])
            #minBefore, minAfter = self.getPeakAdjacences(peakLocation, minT)
            minAux = int(max(minSigma, peakWidths[peak]['leftAdjacence']))#int(max(minSigma, minBefore))
            maxAux = int(min(maxSigma, peakWidths[peak]['rightAdjacence']))#int(min(maxSigma, minAfter))
            distance = min(abs(minAux - peakLocation), abs(maxAux - peakLocation))
            result[peak] = {'position': peakLocation, 'distance': distance}
        return result

    def getPeakAdjacences(self, position, minT):
        #Get the MinTs that surround the desired peak
        minBefore = None
        minAfter = None
        for minpoint in minT:
            if minpoint[0] < position:
                minBefore = minpoint[0]
            else:
                minAfter = minpoint[0]
                break
        if not minBefore or not minAfter:
            raise Exception("Minimuns not found.")
        else:
            return minBefore, minAfter

    def closestNumber(self, number, numberList):
        #Returns the number on the list closest to the number given

        res = None
        distance = 100000
        for closeNumber in numberList:
            if abs(number - closeNumber) < distance:
                distance = abs(number - closeNumber)
                res = closeNumber
        return res

    def fGauss(self, x, center, amplt, width):
        y = np.zeros_like(x, dtype=float)
        x = np.array(x, dtype=float)
        y = y + amplt * np.exp( -((x - center) / width)**2)
        return y

    def integrateGaussians(self, gaussArea, gaussDict, yData):
        #TODO debug e encontrar um trapz que e apenas positivo, curr usando sum
        integration_dict = {}
        for peak_name, value in gaussArea.iteritems():
            real_peak_location = value['position']
            peaks_gauss_info = gaussDict[peak_name]
            x = range(0, self.dataCube.energyLevels)
            peakGaussian = self.fGauss(x, real_peak_location, peaks_gauss_info[real_peak_location]['amplitude'], peaks_gauss_info[real_peak_location]['sigma'])
            mixedGaussians = np.zeros_like(peakGaussian)
            for mixedPeak, mixedValues in peaks_gauss_info.iteritems():
                mixedGaussians += self.fGauss(x, int(mixedPeak), mixedValues['amplitude'], mixedValues['sigma'])

            integration_dict[peak_name] = {}
            integration_dict[peak_name]['intensity'] = np.sum(peakGaussian[real_peak_location - value['distance'] : real_peak_location + value['distance']])#np.trapz(y[real_peak_location - value['distance'] : real_peak_location + value['distance']], dx=self.dataCube.eScale)
            integration_dict[peak_name]['location'] = real_peak_location


            #now calculate the noise
            yDataGaussless = yData - mixedGaussians
            yDataGaussless = np.absolute(yDataGaussless)
            integration_dict[peak_name]['noise'] = np.sum(yDataGaussless[real_peak_location - value['distance'] : real_peak_location + value['distance']])#np.trapz(yDataGaussless[real_peak_location - value['distance'] : real_peak_location + value['distance']], dx=self.dataCube.eScale)


        return integration_dict

    def prettyEDSDecomposition(self, nR1, nR2, sR1, sR2):
        #
        #This is a test method, it serves only to ilustrate the EDS various decomposition methods in present in the class, it also prints pretty
        #This method does violate the 2-tier-approach, so feel free to delete the method/never use it
        #
        mx, mi, ydata, _ = self.processEDS(0, 0, self.dataCube.mHeight, self.dataCube.mWidth, self.dataCube.eOrigin, self.dataCube.energyLevels * self.dataCube.eScale + self.dataCube.eOrigin, nR1, nR2, sR1, sR2)
        print mx
        print mi

        wdata = self.fullWidthAtHalfMaximum(mx, mi, ydata, 0.7, simetry=True)

        import matplotlib.pyplot as plt
        ydata = self._separateBaseLine(0, 0, self.dataCube.mHeight, self.dataCube.mWidth, self.dataCube.eOrigin, self.dataCube.energyLevels * self.dataCube.eScale + self.dataCube.eOrigin)
        ydataorig = self.getSubSpectrum(0, 0, self.dataCube.mHeight, self.dataCube.mWidth, self.dataCube.eOrigin, self.dataCube.energyLevels * self.dataCube.eScale + self.dataCube.eOrigin)

        plt.scatter(np.array(mx)[:, 0], np.array(mx)[: , 1], color='r')
        plt.scatter(np.array(mi)[:, 0], np.array(mi)[: , 1], color='k')
        for pik in wdata:
            plt.scatter(pik[0], ydata[pik[0]], color='m')
            plt.scatter(pik[1], ydata[pik[1]], color='m')

        for wnode in wdata:
            plt.plot(range(wnode[0], wnode[1] + 1), ydata[wnode[0]:wnode[1] + 1], color='r')

        intensities = self.integratePeaks(mx, wdata, ydata)
        for inten in intensities:
            plt.scatter(inten[0], inten[1], color='y')
        #plt.plot(ydata)
        plt.plot(ydataorig)
        plt.plot(ydata, linestyle='--')
        plt.plot(np.zeros(np.size(ydata)))
        plt.show()

    def redoPixel(self, reductionSize, h, w, fR1, fR2, sR1, sR2, reductionMethod=cubeData.DataCube.reduceCube ):
        reductionMethod(self.dataCube, pow(2, reductionSize))
        originalData = self.getSubSpectrum(h, w, h + 1, w + 1, self.dataCube.eOrigin, self.dataCube.energyLevels * self.dataCube.eScale + self.dataCube.eOrigin, True)
        noise = self._getNoiseArray(h, w, h + 1, w + 1, fR1, fR2, sR1, sR2, True, method='linear')
        if abs(np.sum(noise) - 0) <= 0.001:
            print 'There were no Counts in the noise area.'
            return [], [], [], [], [], None
        else:
            bsf = baselineFit.BaselineFit()
            baseline = bsf.baseline_als(originalData, 100000, 0.0005, 100)
            yData = originalData - baseline
            maxt, mint = self._detectPeaks(yData, 4 * noise)
            widthData = self.fullWidthAtHalfMaximum(maxt, mint, yData)
        return maxt, mint, originalData, baseline, widthData, noise

    def redoPixelGauss(self, reductionSize, h, w, fR1, fR2, sR1, sR2, peakList, peakDivergence, reductionMethod=cubeData.DataCube.reduceCube):
        #First, do reduction size 1
        reductionMethod(self.dataCube, pow(2, 0))
        originalData = self.getSubSpectrum(0, 0, 1, 1, self.dataCube.eOrigin, self.dataCube.energyLevels * self.dataCube.eScale + self.dataCube.eOrigin, True)
        noise = self._getNoiseArray(0, 0, 1, 1, fR1, fR2, sR1, sR2, True, method='linear')
        if abs(np.sum(noise) - 0) <= 0.001:
            print 'There were no Counts in the noise area.'
            return [], [], [], [], [], None
        else:
            bsf = baselineFit.BaselineFit()
            baseline = bsf.baseline_als(originalData, 100000, 0.0005, 100)
            yData = originalData - baseline
            maxt, mint = self._detectPeaks(yData, 4 * noise)
            widthData = self.fullWidthAtHalfMaximum(maxt, mint, yData)

            peakWidths = self.getRelevantPeakAreas(np.array((np.array(peakList) - self.dataCube.eOrigin) / self.dataCube.eScale, dtype=int), maxt, widthData, peakDivergence)
            gaussFit = GaussianFit(np.linspace(0, self.dataCube.energyLevels -1, self.dataCube.energyLevels), yData)
            tipDict = gaussFit.fitGaussianList(peakWidths, np.array((np.array(peakList) - self.dataCube.eOrigin) / self.dataCube.eScale, dtype=int))

        #Do reduction size N
        reductionMethod(self.dataCube, pow(2, reductionSize))
        originalData = self.getSubSpectrum(h, w, h + 1, w + 1, self.dataCube.eOrigin, self.dataCube.energyLevels * self.dataCube.eScale + self.dataCube.eOrigin, True)
        noise = self._getNoiseArray(h, w, h + 1, w + 1, fR1, fR2, sR1, sR2, True, method='linear')
        if abs(np.sum(noise) - 0) <= 0.001:
            print 'There were no Counts in the noise area.'
            return [], [], [], [], [], None
        else:
            bsf = baselineFit.BaselineFit()
            baseline = bsf.baseline_als(originalData, 100000, 0.0005, 100)
            yData = originalData - baseline
            maxt, mint = self._detectPeaks(yData, 4 * noise)
            widthData = self.fullWidthAtHalfMaximum(maxt, mint, yData)

            gaussFit = GaussianFit(np.linspace(0, self.dataCube.energyLevels -1, self.dataCube.energyLevels), yData)
            gaussDict = gaussFit.fitGaussianListTips(peakWidths, tipDict)
            gaussArea = self.getGaussianArea(peakWidths, gaussDict, maxt, mint)
            integration_gauss = self.integrateGaussians(gaussArea, gaussDict, yData)
        return maxt, mint, originalData, baseline, widthData, noise, peakWidths, gaussDict, gaussArea, integration_gauss
