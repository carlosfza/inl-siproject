import edsProcessor
import numpy as np
import matplotlib.pyplot as plt
import cubeData
from matplotlib.patches import Ellipse
from matplotlib.widgets import Button
import math
import sys
import matplotlib.lines as mlines

def fGauss(x, center, amplitude, sigma):
    y = np.zeros_like(x)
    y = amplitude * np.exp( -((x - center)/sigma)**2)
    return y

def forceAspect(ax, aspect=1.0):
    im = ax.get_images()
    extent = im[0].get_extent()
    desiredAspect = abs(float(extent[1] - extent[0]) / float(extent[3] - extent[2])) / aspect
    ax.set_aspect(desiredAspect)


class Formatter(object):
    def __init__(self, im):
        self.im = im

    def __call__(self, x, y):
        #z = self.im.get_array()[int(y), int(x)]
        if 0 <= y < np.size(self.im.get_array(), 0) and 0 <= x < np.size(self.im.get_array(), 1):
            z = self.im.get_array()[int(y), int(x)]
            return 'x={}, y={}, Intensity[x, y]={:.01f}'.format(int(x), int(y), z)


class FindAtomResults:
    def __init__(self, resultDict, fR1, fR2, sR1, sR2, edsProc, peakList, peakDivergence, process='atomFinder', reductionMethod=cubeData.DataCube.reduceCube, chartFlag = False):
        self.fR1 = fR1
        self.fR2 = fR2
        self.sR1 = sR1
        self.sR2 = sR2
        self.process = process
        self.edsProc = edsProc
        self.resultDict = resultDict
        if 'reductions' in self.resultDict.keys():
            self.reductions = self.resultDict['reductions']
            del self.resultDict['reductions']
        self.resultKeys = resultDict.keys()
        self.resultKeys.sort()
        self.currentResult = 0
        self.resultSize = len(self.resultKeys)
        self.peakDict = resultDict[self.resultKeys[0]]
        self.index = 0
        self.size = len(self.peakDict)
        self.scale = 'relative'
        self.relativeMax = np.zeros(len(self.peakDict))
        self.relativeMin = np.zeros(len(self.peakDict))
        self.absoluteMax = 0
        self.absoluteMin = np.inf
        self.reductionMethod = reductionMethod
        self.peakList = peakList
        self.peakDivergence = peakDivergence
        self.chartFlag = chartFlag


        #Fill the absolute min and absolute max values
        for peakArray in self.peakDict.values():
            if self.absoluteMax < np.max(peakArray):
                self.absoluteMax = np.max(peakArray)
            if self.absoluteMin > np.min(peakArray):
                self.absoluteMin = np.min(peakArray)

        for i in xrange(0, len(self.peakDict)):
            self.relativeMax[i] = np.max(self.peakDict[i])
            self.relativeMin[i] = np.min(self.peakDict[i])

        #Def fig 2, Figure 2 contains H, which is the weights of the W components
        self.fig2 = plt.figure('2')
        self.ax2 = plt.subplot()

        self.fig2.suptitle('Peak Energy: {} eV - Peak {} of {} - Cube reduction {} of {}'.format(self.resultKeys[self.currentResult], self.currentResult, self.resultSize ,pow(2, i), pow(2, self.size - 1) ))

        extent=[0,len(self.peakDict[0]),len(self.peakDict[0][0]),0]

        self.im = self.ax2.imshow(self.peakDict[0], interpolation='none', extent=[0,len(self.peakDict[0][0]),len(self.peakDict[0]),0])
        self.ax2.format_coord = Formatter(self.im)
        self.im.set_clim(self.relativeMin[0], self.relativeMax[0])
        self.collorBar = self.fig2.colorbar(self.im)

        #Creation of the buttons Next bin & Prev bin Fig1
        axprev = plt.axes([0.69, 0.01, 0.15, 0.045])
        axnext = plt.axes([0.85, 0.01, 0.15, 0.045])
        self.bnext = Button(axnext, 'Next Bin')
        self.bnext.on_clicked(self.next)
        self.bprev = Button(axprev, 'Previous Bin')
        self.bprev.on_clicked(self.prev)

        #Event handling
        self.cid1 = self.fig2.canvas.mpl_connect('button_press_event', self.on_press)

        #Creation of the buttons Next element & Prev element Fig1
        if self.resultSize > 1:
            axprevElem = plt.axes([0.34, 0.01, 0.15, 0.045])
            axnextElem = plt.axes([0.50, 0.01, 0.15, 0.045])
            self.bnextElem = Button(axnextElem, 'Next Element')
            self.bnextElem.on_clicked(self.nextElement)
            self.bprevElem = Button(axprevElem, 'Previous Element')
            self.bprevElem.on_clicked(self.previousElement)
            if self.chartFlag is True:
                axchart = plt.axes([0.43, 0.9, 0.15, 0.045])
                self.bchart = Button(axchart, 'Chart')
                self.bchart.on_clicked(self.on_chart_buttom_press)

        #Creatio Absolute & Relative
        axRelative = plt.axes([0.00, 0.01, 0.15, 0.045])
        axAbsolute = plt.axes([0.16, 0.01, 0.15, 0.045])
        self.bRelative = Button(axRelative, 'Relative')
        self.bRelative.on_clicked(self.relative)
        self.bAbsolute = Button(axAbsolute, 'Absolute')
        self.bAbsolute.on_clicked(self.absolute)

        #print the sizes
        i = self.index % self.size
        print 'Pixel size: {} nm (Height) * {} nm (Width).\nAggregate pixels: {} Height * {} Width.\n' \
              'Ommited pixels: {} rightmost collumns, {} lowermost lines. Note: In order to not have Ommited pixels during the reduction, use images that are size 2^n.'\
              .format(self.reductions[i]['reducedPixelHeightSize'], self.reductions[i]['reducedPixelWidthSize'], self.reductions[i]['reducedAggregateHeight'], self.reductions[i]['reducedAggregateWidth'],
                      self.reductions[i]['reducedMissingWidth'], self.reductions[i]['reducedMissingHeight'])

        plt.sca(self.ax2)
        plt.show()

    def on_chart_buttom_press(self, event):
        currentIndex = self.index % self.size
        fig = plt.figure('spsctrum')
        plt.clf()
        ax = plt.subplot()
        colorIndex = 0
        colorOptions = ['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']
        legendList = []
        for peakName, peakDict in self.resultDict.iteritems():
            if "- Noise" in peakName or 'reductions' in peakName:
                pass
            else:
                colorIndex += 1
                colorIndex = colorIndex % len(colorOptions)
                ax.plot(np.linspace(0, 2 ** currentIndex, 2 ** currentIndex), peakDict[currentIndex][0], colorOptions[colorIndex])

                legendList.append(mlines.Line2D([], [], color=colorOptions[colorIndex], linestyle='-', label=peakName + ' - Peak Intensity'))
                if peakName + ' - Noise' in self.resultDict.keys():
                    legendList.append(mlines.Line2D([], [], color=colorOptions[colorIndex], linestyle='--', label=peakName + ' - Noise Intensity'))
                    ax.plot(np.linspace(0, 2 ** currentIndex, 2 ** currentIndex), self.resultDict[peakName + ' - Noise'][currentIndex][0], colorOptions[colorIndex] + "--")

        #fig.legend(handles=legendList)
        ax.legend(handles=legendList)
        #mlines.Line2D()
        fig.show()

        self.fig2.canvas.draw()


    def on_press(self, event):
        if event.inaxes != self.im.axes: return
        eScale = self.edsProc.dataCube.eScale
        eOrigins = self.edsProc.dataCube.eOrigin
        eLevels = self.edsProc.dataCube.energyLevels
        xData = np.linspace(eOrigins, eOrigins + eScale * eLevels, num=eLevels)
        maxt, mint, originalData, baseline, widthData, noise = self.edsProc.redoPixel(self.index % self.size, event.ydata, event.xdata, self.fR1, self.fR2, self.sR1, self.sR2, self.reductionMethod)


        fig = plt.figure('spsctrum')
        plt.clf()
        ax = plt.subplot()
        if noise is None:
            yData = self.edsProc.getSubSpectrum(event.ydata, event.xdata, event.ydata + 1, event.xdata + 1, eOrigins, eOrigins + eScale * eLevels, reduced=True)
            plt.plot(xData, yData, color='g')
            ax.plot(np.linspace(0, 0, num=eOrigins + eScale * eLevels), color='k')
            fig.show()

            self.fig2.canvas.draw()
            return

        yData = originalData - baseline
        if self.process == 'atomFinder':
            for maxI in maxt:
                ax.scatter(xData[maxI[0]], yData[maxI[0]], color='r')
            for minI in mint:
                ax.scatter(xData[minI[0]], yData[minI[0]], color='k')

            for pik in widthData:
                ax.scatter(xData[pik[0]], yData[pik[0]], color='m')
                ax.scatter(xData[pik[1]], yData[pik[1]], color='m')

            plt.plot(xData, yData, color='g')

            ax.plot(np.linspace(0, 0, num=eOrigins + eScale * eLevels), color='k')
            ax.plot(xData,4 * noise, color='y')
        elif self.process == 'atomProportion':
            maxtO, mintO, _, _, widthDataO, _ = self.edsProc.redoPixel(0, 0, 0, self.fR1, self.fR2, self.sR1, self.sR2, self.reductionMethod)

            for maxI in maxtO:
                ax.scatter(xData[maxI[0]], yData[maxI[0]], color='r')
            for minI in mintO:
                ax.scatter(xData[minI[0]], yData[minI[0]], color='k')

            for pik in widthDataO:
                ax.scatter(xData[pik[0]], yData[pik[0]], color='m')
                ax.scatter(xData[pik[1]], yData[pik[1]], color='m')

            ax.plot(xData[0:widthData[0][0]+1], yData[0:widthData[0][0]+1], color='g')
            ax.plot(xData[widthData[len(widthData) -1][1]: eOrigins + eLevels * eScale], yData[widthData[len(widthData) -1][1]:eLevels], color='g')

            for i in xrange(0, len(widthDataO)):
                ax.plot(np.linspace(xData[widthDataO[i][0]], xData[widthDataO[i][1]], num=(widthDataO[i][1]+1 - widthDataO[i][0])),yData[widthDataO[i][0]:widthDataO[i][1]+1], color='r')
                if i < len(widthDataO) -1:
                    ax.plot(np.linspace(xData[widthDataO[i][1]], xData[widthDataO[i+1][0]], num=(widthDataO[i+1][0]+1 - widthDataO[i][1])),yData[widthDataO[i][1]:widthDataO[i+1][0]+1], color='g')

            intensities = self.edsProc.integratePeaks(maxtO, widthDataO, yData, xData)
            for inten in intensities:
                ax.scatter(xData[inten[0]], inten[1], color='y')
            ax.plot(np.linspace(0, 0, num=eOrigins + eScale * eLevels), color='k')
            ax.plot(xData, 2 * noise, color='y')

        elif self.process == 'atomProportionGauss':
            maxtO, mintO, _, _, widthDataO, _, peakWidths, gaussDict, gaussArea, integration_gauss = self.edsProc.redoPixelGauss(self.index % self.size, event.ydata, event.xdata, self.fR1, self.fR2, self.sR1, self.sR2, self.peakList, self.peakDivergence, self.reductionMethod)

            print integration_gauss

            for maxI in maxt:
                ax.scatter(xData[maxI[0]], yData[maxI[0]], color='r')
            for minI in mint:
                ax.scatter(xData[minI[0]], yData[minI[0]], color='k')

            for position, value in gaussArea.iteritems():
                ax.scatter(xData[value['position'] + value['distance']], yData[value['position'] + value['distance']], color='y')
                ax.scatter(xData[value['position'] - value['distance']], yData[value['position'] - value['distance']], color='y')

            for pik in widthData:
                ax.scatter(xData[pik[0]], yData[pik[0]], color='m')
                ax.scatter(xData[pik[1]], yData[pik[1]], color='m')

            plt.plot(xData, yData, color='g')

            ax.plot(np.linspace(0, 0, num=eOrigins + eScale * eLevels), color='k')
            ax.plot(xData,4 * noise, color='y')

            intensities = self.edsProc.integratePeaks(maxtO, widthDataO, yData, xData)
            for inten in intensities:
                ax.scatter(xData[inten[0]], inten[1], color='y')
            for peak, value_gauss in integration_gauss.iteritems():
                ax.scatter(xData[value_gauss['location']], value_gauss['intensity'], color='m')

            for desiredPeak in gaussDict.itervalues():
                for key, peak in desiredPeak.iteritems():
                    yGauss = fGauss(np.linspace(0, eLevels -1, eLevels), key, peak['amplitude'], peak['sigma'])
                    ax.plot(xData, yGauss, color='b')

        fig.show()

        self.fig2.canvas.draw()

    def next(self, arg):
        self.index += 1
        i = self.index % self.size

        plt.sca(self.ax2)
        plt.cla()
        plt.figure('2')
        self.fig2.suptitle('Peak Energy: {} eV - Peak {} of {} - Cube reduction {} of {}'.format(self.resultKeys[self.currentResult], self.currentResult, self.resultSize ,pow(2, i), pow(2, self.size - 1) ))

        self.im = self.ax2.imshow(self.peakDict[i], interpolation='none', extent=[0,len(self.peakDict[i][0]),len(self.peakDict[i]),0])
        forceAspect(self.ax2)
        self.ax2.format_coord = Formatter(self.im)
        if self.scale == 'relative':
            self.im.set_clim(self.relativeMin[i], self.relativeMax[i])
            self.collorBar.set_clim(self.relativeMin[i], self.relativeMax[i])
        elif self.scale == 'absolute':
            self.im.set_clim(self.absoluteMin, self.absoluteMax)
            self.collorBar.set_clim(self.absoluteMin, self.absoluteMax)

        print 'Pixel size: {} nm (Height) * {} nm (Width).\nAggregate pixels: {} Height * {} Width.\n' \
              'Ommited pixels: {} rightmost collumns, {} lowermost lines. Note: In order to not have Ommited pixels during the reduction, use images that are size 2^n.'\
              .format(self.reductions[i]['reducedPixelHeightSize'], self.reductions[i]['reducedPixelWidthSize'], self.reductions[i]['reducedAggregateHeight'], self.reductions[i]['reducedAggregateWidth'],
                      self.reductions[i]['reducedMissingWidth'], self.reductions[i]['reducedMissingHeight'])
        self.collorBar.draw_all()
        plt.draw()

    def nextElement(self, arg):
        self.currentResult += 1
        self.currentResult = self.currentResult % self.resultSize
        i = self.index % self.size

        self.peakDict = self.resultDict[self.resultKeys[self.currentResult]]

        self.relativeMax = np.zeros(len(self.peakDict))
        self.relativeMin = np.zeros(len(self.peakDict))
        self.absoluteMax = 0
        self.absoluteMin = np.inf

        #Fill the absolute min and absolute max values
        for peakArray in self.peakDict.values():
            if self.absoluteMax < np.max(peakArray):
                self.absoluteMax = np.max(peakArray)
            if self.absoluteMin > np.min(peakArray):
                self.absoluteMin = np.min(peakArray)

        for cont in xrange(0, len(self.peakDict)):
            self.relativeMax[cont] = np.max(self.peakDict[cont])
            self.relativeMin[cont] = np.min(self.peakDict[cont])

        plt.sca(self.ax2)
        plt.cla()
        plt.figure('2')
        self.fig2.suptitle('Peak Energy: {} eV - Peak {} of {} - Cube reduction {} of {}'.format(self.resultKeys[self.currentResult], self.currentResult, self.resultSize ,pow(2, i), pow(2, self.size - 1) ))
        self.im = self.ax2.imshow(self.peakDict[i], interpolation='none', extent=[0,len(self.peakDict[i][0]),len(self.peakDict[i]),0])
        self.ax2.format_coord = Formatter(self.im)
        if self.scale == 'relative':
            self.im.set_clim(self.relativeMin[i], self.relativeMax[i])
            self.collorBar.set_clim(self.relativeMin[i], self.relativeMax[i])
        elif self.scale == 'absolute':
            self.im.set_clim(self.absoluteMin, self.absoluteMax)
            self.collorBar.set_clim(self.absoluteMin, self.absoluteMax)
        self.collorBar.draw_all()
        forceAspect(self.ax2)

        plt.draw()

    def previousElement(self, arg):
        self.currentResult -= 1
        self.currentResult = self.currentResult % self.resultSize
        i = self.index % self.size

        self.peakDict = self.resultDict[self.resultKeys[self.currentResult]]

        self.relativeMax = np.zeros(len(self.peakDict))
        self.relativeMin = np.zeros(len(self.peakDict))
        self.absoluteMax = 0
        self.absoluteMin = np.inf

        #Fill the absolute min and absolute max values
        for peakArray in self.peakDict.values():
            if self.absoluteMax < np.max(peakArray):
                self.absoluteMax = np.max(peakArray)
            if self.absoluteMin > np.min(peakArray):
                self.absoluteMin = np.min(peakArray)

        for cont in xrange(0, len(self.peakDict)):
            self.relativeMax[cont] = np.max(self.peakDict[cont])
            self.relativeMin[cont] = np.min(self.peakDict[cont])

        plt.sca(self.ax2)
        plt.cla()
        plt.figure('2')

        self.fig2.suptitle('Peak Energy: {} eV - Peak {} of {} - Cube reduction {} of {}'.format(self.resultKeys[self.currentResult], self.currentResult, self.resultSize ,pow(2, i), pow(2, self.size - 1) ))
        self.im = self.ax2.imshow(self.peakDict[i], interpolation='none', extent=[0,len(self.peakDict[i][0]),len(self.peakDict[i]),0])
        self.ax2.format_coord = Formatter(self.im)
        if self.scale == 'relative':
            self.im.set_clim(self.relativeMin[i], self.relativeMax[i])
            self.collorBar.set_clim(self.relativeMin[i], self.relativeMax[i])
        elif self.scale == 'absolute':
            self.im.set_clim(self.absoluteMin, self.absoluteMax)
            self.collorBar.set_clim(self.absoluteMin, self.absoluteMax)
        self.collorBar.draw_all()
        forceAspect(self.ax2)

        plt.draw()

    def prev(self, agr):
        self.index -= 1
        i = self.index % self.size

        plt.sca(self.ax2)
        plt.cla()
        plt.figure('2')

        self.fig2.suptitle('Peak Energy: {} eV - Peak {} of {} - Cube reduction {} of {}'.format(self.resultKeys[self.currentResult], self.currentResult, self.resultSize ,pow(2, i), pow(2, self.size - 1) ))
        self.im = self.ax2.imshow(self.peakDict[i], interpolation='none', extent=[0,len(self.peakDict[i][0]),len(self.peakDict[i]),0])
        self.ax2.format_coord = Formatter(self.im)
        if self.scale == 'relative':
            self.im.set_clim(self.relativeMin[i], self.relativeMax[i])
            self.collorBar.set_clim(self.relativeMin[i], self.relativeMax[i])
        elif self.scale == 'absolute':
            self.im.set_clim(self.absoluteMin, self.absoluteMax)
            self.collorBar.set_clim(self.absoluteMin, self.absoluteMax)
        self.collorBar.draw_all()
        forceAspect(self.ax2)

        print 'Pixel size: {} nm (Height) * {} nm (Width).\nAggregate pixels: {} Height * {} Width.\n' \
              'Ommited pixels: {} rightmost collumns, {} lowermost lines. Note: In order to not have Ommited pixels during the reduction, use images that are size 2^n.'\
              .format(self.reductions[i]['reducedPixelHeightSize'], self.reductions[i]['reducedPixelWidthSize'], self.reductions[i]['reducedAggregateHeight'], self.reductions[i]['reducedAggregateWidth'],
                      self.reductions[i]['reducedMissingWidth'], self.reductions[i]['reducedMissingHeight'])

        plt.draw()

    def relative(self, agr):
        if self.scale == 'relative':
            return
        else:
            i = self.index % self.size
            self.scale = 'relative'

            plt.figure('2')
            self.im = self.ax2.imshow(self.peakDict[i], interpolation='none', extent=[0,len(self.peakDict[i][0]),len(self.peakDict[i]),0])
            self.ax2.format_coord = Formatter(self.im)
            self.im.set_clim(self.relativeMin[i], self.relativeMax[i])
            self.collorBar.set_clim(self.relativeMin[i], self.relativeMax[i])
            self.collorBar.draw_all()
            forceAspect(self.ax2)
            plt.draw()

    def absolute(self, agr):
        if self.scale == 'absolute':
            return
        else:
            i = self.index % self.size
            self.scale = 'absolute'

            plt.figure('2')
            self.im = self.ax2.imshow(self.peakDict[i], interpolation='none', extent=[0,len(self.peakDict[i][0]),len(self.peakDict[i]),0])
            self.ax2.format_coord = Formatter(self.im)
            self.im.set_clim(self.absoluteMin, self.absoluteMax)
            self.collorBar.set_clim(self.absoluteMin, self.absoluteMax)
            self.collorBar.draw_all()
            forceAspect(self.ax2)
            plt.draw()

class InteractiveSpectrumCrop:
    def __init__(self, S, eOrigin, eScale):
        #Define the code 0 exit flag
        self.exitFlag = False

        #Figure 2 creation
        self.fig2 = plt.figure('Crop Spectrum')
        self.ax2 = plt.subplot()
        plt.ylabel('counts')
        plt.xlabel('energy (eV)')

        #Variables to save calculation time
        self.max = int(np.max(S))
        self.min = int(np.min(S))

        #Creation of the data
        self.i2 = 0
        self.press2 = None
        self.S = S
        self.eOrigin = eOrigin
        self.eScale = eScale
        self.ex1 = eOrigin + 10
        self.ex2 = eOrigin + (int(self.S.shape[0]) * eScale) * 0.33
        self.ey1 = self.max
        self.ey2 = self.min
        self.e2x1 = eOrigin + (int(self.S.shape[0]) * eScale) * 0.66
        self.e2x2 = eOrigin + (int(self.S.shape[0]) * eScale) * 0.95

        plt.subplots_adjust(bottom=0.2)
        horizontalScale = np.linspace(eOrigin, eOrigin + np.size(self.S, 0) * eScale, np.size(self.S, 0))
        self.l, = self.ax2.plot(horizontalScale, self.S[:], lw=2)
        self.ax2.set_title('Click and Drag the circles to select a peakless area.\nPress finish when done.')
        self.line2, = self.ax2.plot([0], [0], color='#00E600', linewidth=3.0)
        self.line2.set_data([self.ex1, self.ex2, self.ex2, self.ex1, self.ex1], [self.ey1, self.ey1, self.ey2, self.ey2, self.ey1])
        self.line2.figure.canvas.draw()

        self.line3, = self.ax2.plot([0], [0], color='#00E600', linewidth=3.0)
        self.line3.set_data([self.e2x1, self.e2x2, self.e2x2, self.e2x1, self.e2x1], [self.ey1, self.ey1, self.ey2, self.ey2, self.ey1])
        self.line3.figure.canvas.draw()

        #Creation of the circles
        self.posEl1 = abs(self.ax2.get_ylim()[1] - self.ax2.get_ylim()[0]) / 2
        self.ells1 = Ellipse(xy=(self.ex1, self.posEl1), width=abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30, height=abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30 )
        self.ells2 = Ellipse(xy=(self.ex2, self.posEl1), width=abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30, height=abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30 )
        self.ax2.add_artist(self.ells1)
        self.ells1.set_facecolor('r')
        self.ax2.add_artist(self.ells2)
        self.ells2.set_facecolor('r')

        self.ells3 = Ellipse(xy=(self.e2x1, self.posEl1), width=abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30, height=abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30 )
        self.ells4 = Ellipse(xy=(self.e2x2, self.posEl1), width=abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30, height=abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30 )
        self.ax2.add_artist(self.ells3)
        self.ells3.set_facecolor('k')
        self.ax2.add_artist(self.ells4)
        self.ells4.set_facecolor('k')

        #Creation of the coordinates text boxes
        self.textF2P1 = plt.figtext(0.05, 0.05, 'Area#1 > x1 = {}\n        x2 = {}'.format(self.ex1, self.ex2))
        self.textF2P2 = plt.figtext(0.4, 0.05, 'Area#2 > x1 = {}\n        x2 = {}'.format(self.e2x1, self.e2x2))

        #Creation of the continue button
        axcontinue = plt.axes([0.85, 0.1, 0.1, 0.065])
        self.bcontinue = Button(axcontinue, 'Finish')
        self.bcontinue.on_clicked(self.on_continue)

        #Event Handling for figure 2
        self.sid1 = self.line2.figure.canvas.mpl_connect('button_press_event', self.on_press2)
        self.sid2 = self.line2.figure.canvas.mpl_connect('button_release_event', self.on_release2)
        self.sid3 = self.line2.figure.canvas.mpl_connect('motion_notify_event', self.on_motion2)

        plt.show()

    def on_press2(self, event):
        if event.inaxes != self.line2.axes:
            return

        auxRX = abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30
        auxRY = abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30
        if self.inside_elipse(event.xdata, event.ydata, self.ex1, self.posEl1, auxRX, auxRY):
            self.press2 = 1
            self.i2 = 1
        if self.inside_elipse(event.xdata, event.ydata, self.ex2, self.posEl1, auxRX, auxRY):
            self.press2 = 1
            self.i2 = 2
        if self.inside_elipse(event.xdata, event.ydata, self.e2x1, self.posEl1, auxRX, auxRY):
            self.press2 = 1
            self.i2 = 3
        if self.inside_elipse(event.xdata, event.ydata, self.e2x2, self.posEl1, auxRX, auxRY):
            self.press2 = 1
            self.i2 = 4

        self.line2.figure.canvas.draw()

    def on_motion2(self, event):
        if self.press2 is None:
            upperLimit = self.ax2.get_ylim()[1] - abs(self.ax2.get_ylim()[1] - self.ax2.get_ylim()[0]) / 20
            lowerLimit = self.ax2.get_ylim()[0] + abs(self.ax2.get_ylim()[1] - self.ax2.get_ylim()[0]) / 20
            if self.posEl1 > upperLimit:
                self.posEl1 = max(upperLimit, self.ey2)
            if self.posEl1 < lowerLimit:
                self.posEl1 = min(lowerLimit, self.ey1)

            self.ells1.remove()
            self.ells2.remove()
            self.ells3.remove()
            self.ells4.remove()
            self.ells1 = Ellipse(xy=(self.ex1, self.posEl1), width=abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30, height=abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30 )
            self.ells2 = Ellipse(xy=(self.ex2, self.posEl1), width=abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30, height=abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30 )
            self.ax2.add_artist(self.ells1)
            self.ells1.set_facecolor('r')
            self.ax2.add_artist(self.ells2)
            self.ells2.set_facecolor('r')
            self.ells3 = Ellipse(xy=(self.e2x1, self.posEl1), width=abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30, height=abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30 )
            self.ells4 = Ellipse(xy=(self.e2x2, self.posEl1), width=abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30, height=abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30 )
            self.ax2.add_artist(self.ells3)
            self.ells3.set_facecolor('k')
            self.ax2.add_artist(self.ells4)
            self.ells4.set_facecolor('k')
            return
        if event.inaxes != self.line2.axes:
            return
        if self.i2 == 1:
            self.ex1 = max(min(int(event.xdata), self.eOrigin + np.size(self.S, 0) * self.eScale - 1), self.eOrigin)
            self.posEl1 = max(min(event.ydata, self.max), self.min)

        elif self.i2 == 2:
            self.ex2 = max(min(int(event.xdata), self.eOrigin + np.size(self.S, 0) * self.eScale - 1), self.eOrigin)
            self.posEl1 = max(min(event.ydata, self.max), self.min)

        elif self.i2 == 3:
            self.e2x1 = max(min(int(event.xdata), self.eOrigin + np.size(self.S, 0) * self.eScale - 1), self.eOrigin)
            self.posEl1 = max(min(event.ydata, self.max), self.min)

        elif self.i2 == 4:
            self.e2x2 = max(min(int(event.xdata), self.eOrigin + np.size(self.S, 0) * self.eScale - 1), self.eOrigin)
            self.posEl1 = max(min(event.ydata, self.max), self.min)

        self.ells1.remove()
        self.ells2.remove()
        self.ells3.remove()
        self.ells4.remove()
        self.ells1 = Ellipse(xy=(self.ex1, self.posEl1), width=abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30, height=abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30 )
        self.ells2 = Ellipse(xy=(self.ex2, self.posEl1), width=abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30, height=abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30 )
        self.ax2.add_artist(self.ells1)
        self.ells1.set_facecolor('r')
        self.ax2.add_artist(self.ells2)
        self.ells2.set_facecolor('r')
        self.ells3 = Ellipse(xy=(self.e2x1, self.posEl1), width=abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30, height=abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30 )
        self.ells4 = Ellipse(xy=(self.e2x2, self.posEl1), width=abs(self.ax2.get_xlim()[0] - self.ax2.get_xlim()[1]) / 30, height=abs(self.ax2.get_ylim()[0] - self.ax2.get_ylim()[1]) / 30 )
        self.ax2.add_artist(self.ells3)
        self.ells3.set_facecolor('k')
        self.ax2.add_artist(self.ells4)
        self.ells4.set_facecolor('k')

        self.textF2P1.set_text('Area#1 > x1 = {}\n        x2 = {}'.format(self.ex1, self.ex2))
        self.textF2P2.set_text('Area#2 > x1 = {}\n        x2 = {}'.format(self.e2x1, self.e2x2))

        self.line2.set_data([self.ex1, self.ex2, self.ex2, self.ex1, self.ex1], [self.ey1, self.ey1, self.ey2, self.ey2, self.ey1])
        self.line2.figure.canvas.draw()
        self.line3.set_data([self.e2x1, self.e2x2, self.e2x2, self.e2x1, self.e2x1], [self.ey1, self.ey1, self.ey2, self.ey2, self.ey1])
        self.line3.figure.canvas.draw()

    def on_release2(self, event):
        #if event.inaxes != self.line2.axes: return
        self.press2 = None

        self.line2.figure.canvas.draw()

    def on_continue(self, event):
        if self.ex1 > self.ex2:
            aux = self.ex1
            self.ex1 = self.ex2
            self.ex2 = aux
        if self.e2x1 > self.e2x2:
            aux = self.e2x1
            self.e2x1 = self.e2x2
            self.e2x2 = aux

        self.exitFlag = True
        plt.figure('Crop Spectrum')
        plt.close()

    @staticmethod
    def inside_elipse(x, y, h, k, rx, ry):
        return math.pow(x - h, 2) / float(math.pow(rx, 2)) + math.pow(y - k, 2) / float(math.pow(ry, 2)) < 1


class ElementCompositions:
    def __init__(self, dataCube):
        self.noiseRegion1 = None
        self.noiseRegion2 = None
        self.dataCube = dataCube
        self.edsProc = edsProcessor.EDSProcessor(dataCube)

    def defineNoiseRegion(self, nR1, nR2):
        self.noiseRegion1 = nR1
        self.noiseRegion2 = nR2

    def getElementCompositionsEDS(self, nR1, nR2, n2R1, n2R2, reduceSize):
        #TODO create the return of the function
        self.dataCube.reduceCube(reduceSize)

        for h in xrange(0, self.dataCube.reducedHeight):
            for w in xrange(0, self.dataCube.reducedWidth):
                maxt, _, _, _ = self.edsProc.processEDS(h, w, h + 1, w + 1, self.dataCube.eOrigin, self.dataCube.energyLevels * self.dataCube.eScale + self.dataCube.eOrigin, nR1, nR2, n2R1, n2R2, True)

                yData = self.dataCube.reducedCube[h: h + 1, w: w + 1]

                ##plt.plot(edsp._getSubSpectrum(0, 0, dc.mHeight, dc.mWidth, 0, dc.energyLevels))
                yData2 = self.edsProc._separateBaseLine(h, w, h + 1, w + 1, 0, self.dataCube.energyLevels, True)
                plt.scatter(np.array(maxt)[:, 0], np.array(maxt)[:, 1], color='r')
                plt.plot(yData2[:])
                plt.plot(np.zeros(np.size(yData)))
                plt.show()

    def findAtomsEDS(self, maxReduceSize, peakArray, peakDivergence, nR1=None, nR2=None, n2R1=None, n2R2=None, reductionMethodName='N'):
        if nR1 is None or nR2 is None or n2R1 is None or n2R2 is None:
            ic = InteractiveSpectrumCrop(self.dataCube.returnCompleteSpectrum(), self.dataCube.eOrigin, self.dataCube.eScale)
            if ic.exitFlag == 1:
                nR1 = ic.ex1
                nR2 = ic.ex2
                n2R1 = ic.e2x1
                n2R2 = ic.e2x2
            else:
                #Da erro
                print 'Noise region detection failed.'
                return

        #Choose the cube reduction method
        chartButton = False
        if reductionMethodName == 'H':
            reductionMethod = cubeData.DataCube.reduceDataCubeHorizontal
            chartButton = True
        elif reductionMethodName == 'V':
            reductionMethod = cubeData.DataCube.reduceDataCubeVertical
            chartButton = True
        else:
            reductionMethod = cubeData.DataCube.reduceCube
        peakDict = self.edsProc.trackAtomsEDS(nR1, nR2, n2R1, n2R2, maxReduceSize, peakArray, peakDivergence, reductionMethod)
        if peakDict is not None:
            letstrydis = FindAtomResults(peakDict, nR1, nR2, n2R1, n2R2, self.edsProc, peakArray, peakDivergence, 'atomFinder', reductionMethod, chartButton)
        return peakDict

    def atomsProportionsEDS(self, maxReduceSize, peakArray, peakDivergence, nR1=None, nR2=None, n2R1=None, n2R2=None, reductionMethodName='N', gauss=False):
        if nR1 is None or nR2 is None or n2R1 is None or n2R2 is None:
            ic = InteractiveSpectrumCrop(self.dataCube.returnCompleteSpectrum(), self.dataCube.eOrigin, self.dataCube.eScale)
            if ic.exitFlag == 1:
                nR1 = ic.ex1
                nR2 = ic.ex2
                n2R1 = ic.e2x1
                n2R2 = ic.e2x2
            else:
                #Da erro
                print 'Noise region detection failed.'
                return


        #Choose the cube reduction method
        chartButton = False
        if reductionMethodName == 'H':
            reductionMethod = cubeData.DataCube.reduceDataCubeHorizontal
            chartButton = True
        elif reductionMethodName == 'V':
            reductionMethod = cubeData.DataCube.reduceDataCubeVertical
            chartButton = True
        else:
            reductionMethod = cubeData.DataCube.reduceCube
        if gauss:
            process = 'atomProportionGauss'
            peakDict = self.edsProc.atomsProportionsEDSGauss(nR1, nR2, n2R1, n2R2, maxReduceSize, peakArray, peakDivergence, reductionMethod)
        else:
            process = 'atomProportion'
            peakDict = self.edsProc.atomsProportionsEDSV2(nR1, nR2, n2R1, n2R2, maxReduceSize, peakArray, peakDivergence, reductionMethod)

        if peakDict is not None:
            letstrydis = FindAtomResults(peakDict, nR1, nR2, n2R1, n2R2, self.edsProc, peakArray, peakDivergence, process, reductionMethod, chartButton)
        return peakDict


    def selectNoisyRegionToAtoms(self):
        ic = InteractiveSpectrumCrop(self.dataCube.returnCompleteSpectrum(), self.dataCube.eOrigin, self.dataCube.eScale)
        if ic.exitFlag == 1:
            self.getElementCompositionsEDS(ic.ex1, ic.ex2, 1)
        else:
            pass

    def edsProcessorTest(self, nR1=None, nR2=None, n2R1=None, n2R2=None):
        if nR1 is None or nR2 is None:
            ic = InteractiveSpectrumCrop(self.dataCube.returnCompleteSpectrum(), self.dataCube.eOrigin, self.dataCube.eScale)
            if ic.exitFlag == 1:
                nR1 = ic.ex1
                nR2 = ic.ex2
                n2R1 = ic.e2x1
                n2R2 = ic.e2x2
            else:
                #Da erro
                print 'Noise region detection failed.'
                return
        peakDict = self.edsProc.prettyEDSDecomposition(nR1, nR2, n2R1, n2R2)
