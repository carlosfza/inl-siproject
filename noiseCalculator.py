import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import collections
import baselineFit
import math

def power_law(x, c, a, s):
    return c * ((x + s) ** a)


def line(x, a, b):
    return b + (x * a)


class NoiseCalculator:

    def __init__(self, data, origin, data2=None, origin2=np.inf):
        self.data = np.array(data, dtype='float64')
        self.origin = origin
        self.data2 = np.array(data2, dtype='float64')
        self.origin2 = origin2

        self.xdata = np.array(range(1, np.size(data)+1), dtype='float64')
        self.xdata2 = np.array(range(1, np.size(self.data2)+1), dtype='float64')

        self.baseNoise = None
        self.decayNoise = None
        self.poptPL = None
        self.pcovPL = None

        if self.origin > self.origin2:
            aux = self.origin
            self.origin = self.origin2
            self.origin2 = aux
            aux = self.data
            self.data = self.data2
            self.data2 = aux

    def getFit(self, method, dataSet):
        #Returns the function fit.
        #inputs:    method -> name of the function to fit the data, such as 'powerlaw' or 'lognormal'
        #outputs:   res -> tuple containing
        if method == 'powerlaw':
            if dataSet == 1:
                self.poptPL, self.pcovPL = curve_fit(power_law, self.xdata, self.data, p0=[np.max(self.data), -2, self.origin], maxfev=10000)
                return power_law(self.xdata, self.poptPL[0], self.poptPL[1], self.poptPL[2])
            elif dataSet == 2:
                self.poptPL, self.pcovPL = curve_fit(power_law, self.xdata2, self.data2, p0=[np.max(self.data2), -2, self.origin2], maxfev=10000)
                return power_law(self.xdata2, self.poptPL[0], self.poptPL[1], self.poptPL[2])
        if method == 'asymmetricLeastSquares':
            if dataSet == 1:
                bf = baselineFit.BaselineFit()
                return bf.baseline_als(self.data, 10000000, 0.5, 100)
            elif dataSet == 2:
                bf = baselineFit.BaselineFit()
                return bf.baseline_als(self.data2, 10000000, 0.5, 100)
        if method == 'linear':
            if dataSet == 1:
                self.poptPL, self.pcovPL = curve_fit(line, self.xdata, self.data, p0=[np.max(self.data), -0.001], maxfev=10000)
                #Prepare fit function
                return line(self.xdata, self.poptPL[0], self.poptPL[1])
            elif dataSet == 2:
                self.poptPL, self.pcovPL = curve_fit(line, self.xdata2, self.data2, p0=[np.max(self.data2), -0.001], maxfev=10000)
                #Prepare fit function
                return line(self.xdata2, self.poptPL[0], self.poptPL[1])
        elif method == 'lognormal':
            raise NameError('No method with the provided name.')
            return -1
        else:
            raise NameError('No method with the provided name.')
            return -1

    def getPowerNoise(self, method):
        if method == 'powerlaw':
            #Perform the fitting
            self.poptPL, self.pcovPL = curve_fit(power_law, self.xdata, self.data, p0=[np.max(self.data), -2, self.origin], maxfev=10000)
            #Prepare fit function
            fit = power_law(self.xdata, self.poptPL[0], self.poptPL[1], self.poptPL[2])
            #Ajust the fit so that the fit is never greater than the original data
            fit = fit + min(self.data - fit)
            #Calculate the maximum deviation between the fit and the signal
            noisePower = np.max(np.abs(self.data - fit))
            return noisePower
        elif method == 'asymmetricLeastSquares':
            #Create the baseline, since there are suposetely no peaks, it should diverge only on noise

            #Ajust the baseline so that it is never greater than the data

            #Calculate the maximum deviation
            #TODO: test the best als parameters and implement the method
            raise NameError('No method with the provided name.')
            return -1
        elif method == 'linear':
            #Perform the fitting
            self.poptPL, self.pcovPL = curve_fit(line, self.xdata, self.data, p0=[np.max(self.data), -0.001], maxfev=10000)
            #Prepare fit function
            fit = line(self.xdata, self.poptPL[0], self.poptPL[1])
            #Ajust the fit so that the fit is never greater than the original data
            fit = fit + min(self.data - fit)
            #Calculate the maximum deviation between the fit and the signal
            noisePower = np.max(np.abs(self.data - fit))
            return noisePower
        else:
            raise NameError('No method with the provided name.')
            return -1

    def calculateNoiseValues(self, method, scaling='energy'):
        #Calculate a fit of the two areas
        fit1 = self.getFit(method, 1)
        fit2 = self.getFit(method, 2)
        baseline1 = fit1 + np.min(self.data - fit1)
        baseline2 = fit2 + np.min(self.data2 - fit2)

        #Calculate Standard deviation
        sumSquares1 = 0
        for i in xrange(0, np.size(fit1)):
            sumSquares1 += (fit1[i] - self.data[i]) ** 2

        sumSquares2 = 0
        for i in xrange(0, np.size(fit2)):
            sumSquares2 += (fit2[i] - self.data2[i]) ** 2

        sumSquares1 = float(sumSquares1) / float(np.size(fit1) - 1)
        sumSquares2 = float(sumSquares2) / float(np.size(fit2) - 1)

        deviation1 = math.sqrt(sumSquares1)
        deviation2 = math.sqrt(sumSquares2)

        if scaling == 'baseline':
            average1 = float(np.sum(baseline1)) / float(np.size(baseline1))
            average2 = float(np.sum(baseline2)) / float(np.size(baseline2))

            if average1 - average2 == 0:
                raise ValueError('Noise Calculation - No counts found on one of the areas or the average is the same.')

            self.decayNoise = float(deviation1 - deviation2) / float(average1 - average2)
            self.baseNoise = deviation1 - average1 * self.decayNoise
            print 'Decay Noise ',
            print self.decayNoise
            print 'Base Noise ',
            print self.baseNoise

        elif scaling == 'energy':
            average1 = float(np.size(self.data)) / 2.0 + self.origin
            average2 = float(np.size(self.data2)) / 2.0 + self.origin2

            if average1 == 0 or average2 == 0:
                raise ValueError('Noise Calculation - No counts found on one of the areas.')

            self.decayNoise = float(deviation1 - deviation2) / float(average1 - average2)
            self.baseNoise = deviation1 - average1 * self.decayNoise

            print 'Decay Noise ',
            print self.decayNoise
            print 'Base Noise ',
            print self.baseNoise

        elif scaling == 'intensity':
            average1 = float(np.sum(self.data)) / float(np.size(self.data))
            average2 = float(np.sum(self.data2)) / float(np.size(self.data2))

            if average1 == 0 or average2 == 0:
                raise ValueError('Noise Calculation - No counts found on one of the areas.')

            self.decayNoise = float(deviation1 - deviation2) / float(average1 - average2)
            self.baseNoise = deviation1 - average1 * self.decayNoise

            print 'Decay Noise ',
            print self.decayNoise
            print 'Base Noise ',
            print self.baseNoise

    def calculateArrayNoise(self, array, method=None, scaling='energy'):
        if method is not None:
            try:
                self.calculateNoiseValues(method, scaling)
            except ValueError as e:
                res = np.zeros(np.size(array))
                return res

        res = np.zeros(np.size(array))

        if scaling == 'energy':
            for i in xrange(0, np.size(array)):
                res[i] = max(0, i * self.decayNoise + self.baseNoise)
        elif scaling == 'baseline':
            bf = baselineFit.BaselineFit()
            baseline = bf.baseline_als(array, 1000000, 0.0005, 100)
            for i in xrange(0, np.size(array)):
                res[i] = max(0, baseline[i] * self.decayNoise + self.baseNoise)
        elif scaling == 'intensity':
            for i in xrange(0, np.size(array)):
                res[i] = max(0, array[i] * self.decayNoise + self.baseNoise)

        return res