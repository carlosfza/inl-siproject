import collections
import matplotlib.pyplot as plt
import numpy as np
from scipy import sparse
from scipy.sparse.linalg import spsolve
import pylab as plb
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
import matplotlib.pyplot as plt


def highOrderFunction(numberGauss, posVector, sigmaVector):
    def fGauss(x, *params):
        y = np.zeros_like(x)
        for i in range(0, len(params)):
            ctr = float(posVector[i])
            amp = float(params[i])
            wid = float(sigmaVector[i])
            y = y + amp * np.exp( -((x - ctr)/wid)**2)
        return y
    return fGauss


def func(x, *params):
    y = np.zeros_like(x)
    for i in range(0, len(params), 3):
        ctr = params[i]
        amp = params[i+1]
        wid = params[i+2]
        y = y + amp * np.exp( -((x - ctr)/wid)**2)
    return y

# x = np.array(range(0,100))
# gauss = highOrderFunction(3, [30, 50, 60], [5, 15, 1])
# y = np.array(gauss(x, 50, 100, 30))
# noise = np.random.rand(100)*3
# ynoise = y + noise
# plt.plot(x, ynoise)
# guess = [np.array([-1, -1, -1])]
# popt, pcov = curve_fit(func, x, ynoise, p0=[1,1,1,1,1,1,1,1,1], maxfev=100000)
# print popt
# fit = func(x, *popt)
# for i in range(0, len(popt), 3):
#     plt.plot(x, func(x, popt[i], popt[i+1], popt[i+2]))
# plt.show()



#Teste que mostra que o trapz nao e adequado
# x = np.zeros(1000)
# #x = np.array(x) + 1
# for i in range(0,999):
#     if i % 2 == 0:
#         x[i] = -1
#     else:
#         x[i] = 1
# print sum(x)
# xpositive = map((lambda x: 0 if x < 0 else x), x)
# xnegative = map((lambda x: -x if x < 0 else 0), x)
# print sum(xnegative)
# print sum(xpositive)
# print np.trapz(xpositive)
# print np.trapz(xnegative)
# print np.trapz(xpositive) + np.trapz(xnegative)
# print np.trapz(x)

dictpic = {}
dictpic['one'] = 'none'
if 'none' in dictpic:
    print 'incorrect'
if 'one' in dictpic:
    print dictpic['one']