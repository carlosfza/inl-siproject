import DM3lib as dm3
import matplotlib.pyplot as plt
import numpy as np
import re
import rplParser


# noinspection PyAttributeOutsideInit
class DataCube:
    def dumpCube(self, fileout):
        fw = open(fileout, 'w')
        for col in self.pixelMatrix:
            for pixel in col:
                for valueD in pixel:
                    fw.write(str(valueD))
                    fw.write(' ')
                fw.write('\n')

        fw.close()

    def dumpCubeReduction(self, fileout):
        fw = open(fileout, 'w')
        for col in self.reducedCube:
            for pixel in col:
                for valueD in pixel:
                    fw.write(str(valueD))
                    fw.write(' ')
                fw.write('\n')

        fw.close()

    def printStats(self):
        print 'Data cube statistics:'
        print self.pixelMatrix.shape
        print("Cube size: {} x {} (height x width)".format(self.mHeight, self.mWidth))
        print('Energy levels: {}'.format(self.energyLevels))
        print('Energy Scale: {}'.format(self.eScale))
        print('Energy Origin: {}'.format(self.eOrigin))
        # if there is a reduced cube, print the reduced cube stats
        if hasattr(self, 'reducedCube'):
            print '===============\nReduced Data Cube:'
            print("Reduced Cube size: {} x {} (height x width)".format(self.reducedHeight, self.reducedWidth))

    def returnAgregatedMatrix(self):
        i = 0
        res = np.zeros((self.energyLevels, self.mHeight*self.mWidth), np.float64)
        for x in xrange(0, np.size(self.pixelMatrix, 0)):
            for y in xrange(0, np.size(self.pixelMatrix, 1)):
                res[:, i] = self.pixelMatrix[x, y, :]
                i += 1
        return res

    def returnAgregatedReduced(self):
        i = 0
        res = np.zeros((self.energyLevels, self.reducedHeight*self.reducedWidth), np.float64)
        for x in xrange(0, np.size(self.reducedCube, 0)):
            for y in xrange(0, np.size(self.reducedCube, 1)):
                res[:, i] = self.reducedCube[x, y, :]
                i += 1
        return res

    def returnCompleteSpectrum(self):
        # Sum all the pixels
        completeSpectrum = np.zeros((self.energyLevels))
        for line in self.pixelMatrix:
            for pixel in line:
                completeSpectrum = completeSpectrum + pixel
        return completeSpectrum

    def returnCompleteImage(self):
        completeImage = np.zeros((self.mHeight, self.mWidth))
        for x in xrange(0, self.mHeight):
            for y in xrange(0, self.mWidth):
                completeImage[x,y] = np.sum(self.pixelMatrix[x,y,:])
        return completeImage

    def getEnergyPos(self, e):
        #This function converts energy level (in eV) to the position in the DataCube.pixelMatrix array.
        #The conversion takes into account the energy origin and the scale.
        #For example, if the scale is 0.1 eV intervals and 10 eV origin, then e = 13 will be the 31th position in the pixel matrix array, indexed by 30
        return (e - self.eOrigin) // self.eScale


    def displaySpectrumAverage(self, h1, w1, h2, w2):
        if h1 < self.mHeight and h1 >= 0 and w1 >= 0 and w1 < self.mWidth and h2 < self.mHeight and h2 >= 0 and w2 >= 0 and w2 < self.mWidth and h1 <= h2 and w1 <= w2:

            # Get all pixels from the area
            pixelList = []
            for x in xrange(h1, h2 + 1):
                for y in xrange(w1, w2 + 1):
                    pixelList.append(self.pixelMatrix[x][y])

            # Sum all components
            averageSpectrum = np.zeros(self.energyLevels)
            for pixel in pixelList:
                averageSpectrum = averageSpectrum + pixel

            # Divide by the number of pixels used
            averageSpectrum /= len(pixelList)

            # Plot the final Result
            r = np.linspace(self.eOrigin, self.eOrigin + self.energyLevels * self.eScale, self.energyLevels)
            plt.plot(r, averageSpectrum)
            plt.ylabel('counts')
            plt.xlabel('energy')
            plt.show()

            return True
        else:
            return False

    def displaySpectrumSum(self, h1, w1, h2, w2):
        if h1 < self.mHeight and h1 >= 0 and w1 >= 0 and w1 < self.mWidth and h2 < self.mHeight and h2 >= 0 and w2 >= 0 and w2 < self.mWidth and h1 <= h2 and w1 <= w2:

            # Get all pixels from the area
            pixelList = []
            for x in xrange(h1, h2 + 1):
                for y in xrange(w1, w2 + 1):
                    pixelList.append(self.pixelMatrix[x][y])

            # Sum all the pixels
            sumSpectrum = np.zeros(self.energyLevels)
            for pixel in pixelList:
                sumSpectrum = sumSpectrum + pixel

            r = np.linspace(self.eOrigin, self.eOrigin + self.energyLevels * self.eScale, self.energyLevels)
            plt.plot(r, sumSpectrum)
            plt.ylabel('counts')
            plt.xlabel('energy')
            plt.show()

            return True
        else:
            return False

    def displaySpectrum(self, height, width):
        if height < self.mHeight and height >= 0 and width >= 0 and width < self.mWidth:
            r = np.linspace(self.eOrigin, self.eOrigin + self.energyLevels * self.eScale, self.energyLevels)
            plt.plot(r, self.pixelMatrix[height][width])
            plt.ylabel('counts')
            plt.xlabel('energy')
            plt.show()

            return True
        else:
            return False

    def displayImage(self, energy):
        if energy >= self.eOrigin and energy <= self.eOrigin + self.eScale * self.energyLevels:
            imaux = self.pixelMatrix[:, :, (energy - self.eOrigin) // self.eScale:(energy - self.eOrigin) // self.eScale + 1]
            imaux = np.reshape(imaux, (self.mHeight, self.mWidth))
            imgplot = plt.imshow(imaux, cmap=plt.cm.gray, interpolation='none')
            imgplot.set_cmap('spectral')
            plt.colorbar()
            plt.show()
            return True
        else:
            return False

    def displayImageRange(self, energy, energy2):
        if energy >= self.eOrigin and energy < self.eOrigin + self.eScale * self.energyLevels and energy2 >= self.eOrigin and energy2 < self.eOrigin + self.eScale * self.energyLevels and energy < energy2:
            e1Pos = int((energy - self.eOrigin) // self.eScale)
            e2Pos = int((energy2 - self.eOrigin) // self.eScale)
            imfinal = np.zeros((self.mHeight, self.mWidth))
            for x in xrange(e1Pos, e2Pos):
                imaux = self.pixelMatrix[:, :, x:x + 1]
                imaux = np.reshape(imaux, (self.mHeight, self.mWidth))
                imfinal += imaux

            imgplot = plt.imshow(imfinal, cmap= plt.cm.gray, interpolation='none')
            imgplot.set_cmap('spectral')
            plt.colorbar()
            plt.show()
            return True
        else:
            return False

    def displayImageRangeReduced(self, energy, energy2):
        if energy >= self.eOrigin and energy < self.eOrigin + self.eScale * self.energyLevels and energy2 >= self.eOrigin and energy2 < self.eOrigin + self.eScale * self.energyLevels and energy < energy2:
            e1Pos = int((energy - self.eOrigin) // self.eScale)
            e2Pos = int((energy2 - self.eOrigin) // self.eScale)
            imfinal = np.zeros((self.reducedHeight, self.reducedWidth))
            for x in xrange(e1Pos, e2Pos):
                imaux = self.reducedCube[:, :, x:x + 1]
                imaux = np.reshape(imaux, (self.reducedHeight, self.reducedWidth))
                imfinal += imaux

            import numpy
            numpy.set_printoptions(threshold=numpy.nan)
            print imfinal
            imgplot = plt.imshow(imfinal, cmap=plt.cm.gray, interpolation='none')
            imgplot.set_cmap('spectral')
            plt.colorbar()
            plt.show()
            return True
        else:
            return False

    def reduceCube(self, reductionSize):
        self.reducedHeight = reductionSize
        self.reducedWidth = reductionSize

        # Create the reduced cube with appropriate dimensions
        self.reducedCube = np.zeros((self.reducedHeight, self.reducedWidth, self.energyLevels))

        # Place new values into cube
        xaux = self.mHeight // reductionSize
        yaux = self.mWidth // reductionSize

        self.reducedAggregateHeight = xaux
        self.reducedAggregateWidth = yaux
        self.reducedMissingHeight = self.mHeight % reductionSize
        self.reducedMissingWidth = self.mWidth % reductionSize

        for x in xrange(0, self.mHeight):
            for y in xrange(0, self.mWidth):
                if x // xaux < reductionSize and y // yaux < reductionSize:
                    self.reducedCube[x // xaux][y // yaux] = self.reducedCube[x // xaux][y // yaux] + self.pixelMatrix[x][y]

    def setCalibrations(self, scale, origin, heightSize=1, widthSize=1):
        self.eScale = scale
        self.eOrigin = origin
        self.pixelHeightSize = heightSize
        self.pixelWidthSize = widthSize

    def cropDataCube(self, h1, w1, h2, w2, e1, e2):
        # Check if the dataCube dimensions permit the crop
        if h1 < self.mHeight and h1 >= 0 and w1 >= 0 and w1 < self.mWidth and h2 < self.mHeight and h2 >= 0 and w2 >= 0 and w2 < self.mWidth and h1 <= h2 and w1 <= w2:
            # Check if the energy spectrum permits the crop
            if e1 >= self.eOrigin and e1 < (self.eOrigin + self.eScale * self.energyLevels) and e2 >= self.eOrigin and e2 < (self.eOrigin + self.eScale * self.energyLevels) and e1 < e2:
                newEnergyLevel = int((e2 - e1) // self.eScale)
                startingPos = int((e1 - self.eOrigin) // self.eScale)

                # Crop the DataCube
                newDataCube = self.pixelMatrix[h1:(h2 + 1), w1:(w2 + 1), startingPos:(startingPos + newEnergyLevel)]    #Ter cuidado com o +1

                # Update the DataCube Statistics
                self.mHeight = h2 - h1 + 1          #Ter cuidado com o +1
                self.mWidth = w2 - w1 + 1           #ter cuidado com +1
                self.energyLevels = newEnergyLevel
                self.eOrigin = e1
                self.pixelMatrix = newDataCube

                return True
        return False

    def binDataCube(self, size):
        self.reducedHeight = self.mHeight // size
        self.reducedWidth = self.mWidth // size

        self.reducedAggregateHeight = size
        self.reducedAggregateWidth = size
        self.reducedMissingHeight = self.mHeight % size
        self.reducedMissingWidth = self.mWidth % size

        binnedCube = np.zeros((self.reducedHeight, self.reducedWidth, self.energyLevels))
        for x in xrange(0, self.mHeight):
            for y in xrange(0, self.mWidth):
                if x // size < self.reducedHeight and y // size < self.reducedWidth:
                    binnedCube[x // size][y // size] += self.pixelMatrix[x][y]

        self.reducedCube = binnedCube
        return True

    def reduceDataCubeHorizontal(self, reductionSize):
        self.reducedHeight = 1
        self.reducedWidth = reductionSize

        # Create the reduced cube with appropriate dimensions
        self.reducedCube = np.zeros((self.reducedHeight, self.reducedWidth, self.energyLevels))

        # Place new values into cube
        yaux = self.mWidth // reductionSize

        self.reducedAggregateHeight = self.mHeight
        self.reducedAggregateWidth = yaux
        self.reducedMissingHeight = 0
        self.reducedMissingWidth = self.mWidth % reductionSize

        for x in xrange(0, self.mHeight):
            for y in xrange(0, self.mWidth):
                if y // yaux < reductionSize:
                    self.reducedCube[0][y // yaux] = self.reducedCube[0][y // yaux] + self.pixelMatrix[x][y]

    def reduceDataCubeVertical(self, reductionSize):
        self.reducedHeight = reductionSize
        self.reducedWidth = 1

        # Create the reduced cube with appropriate dimensions
        self.reducedCube = np.zeros((self.reducedHeight, self.reducedWidth, self.energyLevels))

        # Place new values into cube
        xaux = self.mHeight // reductionSize

        self.reducedAggregateHeight = xaux
        self.reducedAggregateWidth = self.mWidth
        self.reducedMissingHeight = self.mHeight % reductionSize
        self.reducedMissingWidth = 0

        for x in xrange(0, self.mHeight):
            for y in xrange(0, self.mWidth):
                if x // xaux < reductionSize:
                    self.reducedCube[x // xaux][0] = self.reducedCube[x // xaux][0] + self.pixelMatrix[x][y]



    def __init__(self, filepath):
        """DataCube object, stores information on the current data cube. Implements transformations"""
        if re.match('.+\.rpl$', filepath):
            # Read the file as a rpl file
            # Create the cleaned up file path
            pathArray = re.split('\.rpl$', filepath)

            # Create the Parser Class, pass the file path
            rParser = rplParser.RplParser(pathArray[0])

            # Update class variables, data uses numpy.array
            self.energyLevels = rParser.energyLevels
            self.mHeight = int(rParser.mHeight)
            self.mWidth = int(rParser.mWidth)
            # No scale parameters are usually passed, therefore the scale taken is the default scale
            self.eScale = 1
            self.eOrigin = 0
            self.pixelHeightSize=1
            self.pixelWidthSize=1

            self.pixelMatrix = rParser.data

        elif re.match('.+\.dm3$', filepath):
            # Read the file as dm3 file
            dm3f = dm3.DM3(filepath)
            dm3f.dumpTags()

            # Definition of variables
            energyArray = np.array(dm3f.imageListData)
            energyLevels = len(energyArray)
            if 'root.ImageList.1.ImageData.Calibrations.Dimension.2.Scale' in dm3f.tags and 'root.ImageList.1.ImageData.Calibrations.Dimension.2.Origin' in dm3f.tags:
                self.eScale = float(dm3f.tags['root.ImageList.1.ImageData.Calibrations.Dimension.2.Scale'])
                self.eOrigin = float(dm3f.tags['root.ImageList.1.ImageData.Calibrations.Dimension.2.Origin']) * (-1) * self.eScale
            else:
                self.eScale = 1
                self.eOrigin = 0
            if energyLevels > 0:
                self.energyLevels = energyLevels
                self.mHeight = len(energyArray[0])
                self.mWidth = len(energyArray[0][0])
            else:
                self.energyLevels = 0
                self.mWidth = 0
                self.mHeight = 0

            self.pixelMatrix = np.zeros((self.mHeight, self.mWidth, self.energyLevels))

            for haux in xrange(0, self.mHeight):
                for waux in xrange(0, self.mWidth):
                        self.pixelMatrix[haux, waux, :] = energyArray[:, haux, waux]

            #We are currently not reading pixel size information, so it gets defaulted
            #TODO implement that in case we discover that dm3 format contains pixel sizes
            self.pixelHeightSize=1
            self.pixelWidthSize=1
        elif re.match('.+\.txt$', filepath):
            # Read the file as a txt file
            # Read the txt data
            pixelMatrix_ = np.loadtxt(filepath, delimiter=",")

            self.pixelMatrix = np.zeros((np.size(pixelMatrix_, 0), 1, np.size(pixelMatrix_, 1)))
            for x in xrange(0, np.size(pixelMatrix_, 0)):
                self.pixelMatrix[x, 0, :] += pixelMatrix_[x, :]

            # Update class variables, data uses numpy.array
            self.mHeight = (np.size(self.pixelMatrix, 0))
            self.mWidth = (np.size(self.pixelMatrix, 1))
            self.energyLevels = (np.size(self.pixelMatrix, 2))
            # No scale parameters are usually passed, therefore the scale is noted as the default scale
            self.eScale = 1
            self.eOrigin = 0
            self.pixelHeightSize=1
            self.pixelWidthSize=1
