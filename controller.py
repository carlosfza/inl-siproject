import cubeData as cd
import numpy as np
import re
import decomposer
import renderCrop as rC
import elementCompositions

print 'Welcome to the Controller.\nType a command to begin:'
decomp = decomposer.Decomposer()
userInput = raw_input('')
#state Array
state = np.zeros(2)
while userInput != 'exit':

	if userInput == 'load':
		fpath = raw_input('Please type the file name:\n>')
		if fpath != '':
			dataCube = cd.DataCube(fpath)
			state = np.zeros(2)
			state[0] = 1
			print 'Data Cube loaded successfully.'
		else:
			print 'Please input a file path.'
	elif userInput == 'printStats' or userInput == 'ps':
		if state[0] >= 1:
			dataCube.printStats()
			print 'Success.'
		else:
			print 'No Data Cube is loaded, please use the load command.'
	elif userInput == 'reduceCube' or userInput == 'rc':
		if state[0] >= 1:
			reductionSize = int(raw_input('Please type the reduction size: (size x size)\n>'))
			dataCube.reduceCube(reductionSize)
			state[1] = 1
			print 'Success.'
		else:
			print 'No Data Cube is loaded, please use the load command.'
	elif userInput == 'displaySpectrum' or userInput == 'ds':
		if state[0] >= 1:
			hw = raw_input('Please type the pixel coordinates: (height width)\n>')
			hwarray = re.split('\D+', hw)
			print hw,
			print hwarray[0],
			print hwarray[1]
			res = dataCube.displaySpectrum(int(hwarray[0]), int(hwarray[1]))
			if res:
				print 'Success.'
			else:
				print 'Failed, please check the provived dimensions.'
		else:
			print 'No Data Cube is loaded, please use the load command.'
	elif userInput == 'displaySpectrumAverage' or userInput == 'dsa':
		if state[0] >= 1:
			hw = raw_input('Please type two pixel coordinates: (height width height width)\n>')
			hwarray = re.split('\D+', hw)
			res = dataCube.displaySpectrumAverage(int(hwarray[0]), int(hwarray[1]), int(hwarray[2]), int(hwarray[3]))
			if res:
				print 'Success.'
			else:
				print 'Failed, please check the provived dimensions.'
		else:
			print 'No Data Cube is loaded, please use the load command.'
	elif userInput == 'displaySpectrumSum' or userInput == 'dss':
		if state[0] >= 1:
			hw = raw_input('Please type two pixel coordinates: (height width height width)\n>')
			hwarray = re.split('\D+', hw)
			if(len(hwarray)) == 4:
				res = dataCube.displaySpectrumSum(int(hwarray[0]), int(hwarray[1]), int(hwarray[2]), int(hwarray[3]))
			else:
				print 'Invalid Values.'
			if res:
				print 'Success.'
			else:
				print 'Failed, please check the provived dimensions.'
		else:
			print 'No Data Cube is loaded, please use the load command.'
	elif userInput == 'dumpCube' or userInput == 'dc':
		if state[0] >= 1:
			path = raw_input('Please type the file name:\n>')
			dataCube.dumpCube('./' + path)
			print 'Success.'
		else:
			print 'No Data Cube is loaded, please use the load command.'
	elif userInput == 'dumpCubeReduction' or userInput == 'dcr':
		if state[0] >= 1:
			path = raw_input('Please type the file name:\n>')
			dataCube.dumpCubeReduction('./' + path)
			print 'Success.'
		else:
			print 'No Data Cube is loaded, please use the load command.'
	elif userInput == 'setCalibrations' or userInput == 'sc':
		if state[0] >= 1:
			i = raw_input('Please type the calibration values: (float-scale float-origin <float-pixelSize-height float-pixelSize-width>)\n>')
			iarray = re.findall('[-+]?\d*\.\d+|[-+]?\d+', i)
			if len(iarray) == 2:
				dataCube.setCalibrations(float(iarray[0]), float(iarray[1]))
				print 'Success.'
			elif len(iarray) == 4:
				dataCube.setCalibrations(float(iarray[0]), float(iarray[1]), float(iarray[2]), float(iarray[3]))
				print 'Success.'
			else:
				print 'Invalid calibration values.'
		else:
			print 'No Data Cube is loaded, please use the load command.'
	elif userInput == 'cropDataCube' or userInput == 'cdc':
		if state[0] >= 1:
			i = raw_input('Please type the new coordinate values for the crop: (int-h1 int w1 int-h2 int-w2 float-el1 float-el2)\n>')
			iarray = re.findall('[-+]?\d*\.\d+|[-+]?\d+', i)
			if len(iarray) == 6:
				res = dataCube.cropDataCube(int(iarray[0]), int(iarray[1]), int(iarray[2]), int(iarray[3]), float(iarray[4]), float(iarray[5]))
				if res:
					print 'Success.'
				else:
					print 'Crop Failed.'
			else:
				print 'Invalid crop values.'
		else:
			print 'No Data Cube is loaded, please use the load command.'
	elif userInput == 'displayImage' or userInput == 'di':
		if state[0] >= 1:
			i = raw_input('Please type the energy value: (float-el)\n>')
			iarray = re.findall('[-+]?\d*\.\d+|[-+]?\d+', i)
			if len(iarray) == 1:
				res = dataCube.displayImage(float(iarray[0]))
				if res:
					print 'Success.'
				else:
					print 'Display Failed'
			else:
				print 'Invalid crop values.'
		else:
			print 'No Data Cube is loaded, please use the load command.'
	elif userInput == 'displayImageRange' or userInput == 'dir':
		if state[0] >= 1:
			i = raw_input('Please type the energy range: (float-el float-el2)\n>')
			iarray = re.findall('[-+]?\d*\.\d+|[-+]?\d+', i)
			if len(iarray) == 2:
				res = dataCube.displayImageRange(float(iarray[0]), float(iarray[1]))
				if res:
					print 'Success.'
				else:
					print 'Display Failed'
			else:
				print 'Invalid crop values.'
		else:
			print 'No Data Cube is loaded, please use the load command.'
	elif userInput == 'displayImageRangeReduced' or userInput == 'dirr':
		if state[0] >= 1 and state[1] >= 1:
			i = raw_input('Please type the energy range: (float-el float-el2)\n>')
			iarray = re.findall('[-+]?\d*\.\d+|[-+]?\d+', i)
			if len(iarray) == 2:
				res = dataCube.displayImageRangeReduced(float(iarray[0]), float(iarray[1]))
				if res:
					print 'Success.'
				else:
					print 'Display Failed'
			else:
				print 'Invalid crop values.'
		else:
			print 'No Data Cube is loaded, please use the load command.'
	elif userInput == 'binDataCube' or userInput == 'bdc':
		if state[0] >= 1:
			i = raw_input('Please type the binning size: (integer)\n>')
			iarray = re.findall('\d+', i)
			if len(iarray) == 1:
				res = dataCube.binDataCube(int(iarray[0]))
				if res:
					state[1] = 1
					print 'Success.'
				else:
					print 'Binning Failed'
			else:
				print 'Invalid size value.'
		else:
			print 'No Data Cube is loaded, please use the load command.'
	elif userInput == 'pgnmfDataCube' or userInput == 'pgd':
		if state[0] >= 1:
			i = raw_input('Please type rank and number of iterations. (int int)\n>')
			iarray = re.findall('\d+', i)
			if len(iarray) == 2:
				decomp.pgDataSetRandom(dataCube, int(iarray[0]), int(iarray[1]))
			else:
				print 'Invalid rank or number of iterations.'
		else:
			print 'No Data Cube is loaded, please use the load command.'
	elif userInput == 'lnmfDataCube' or userInput == 'ld':
		if state[0] >= 1:
			i = raw_input('Please type rank and number of iterations. (int int)\n>')
			iarray = re.findall('\d+', i)
			if len(iarray) == 2:
				decomp.lnmfDataSetRandom(dataCube, int(iarray[0]), int(iarray[1]))
			else:
				print 'Invalid rank or number of iterations.'
		else:
			print 'No Data Cube is loaded, please use the load command.'
	elif userInput == 'lnmfReducedCube' or userInput == 'lr':
		if state[0] >= 1 and state[1] >= 1:
			i = raw_input('Please type rank and number of iterations. (int int)\n>')
			iarray = re.findall('\d+', i)
			if len(iarray) == 2:
				decomp.lnmfDataSetRandomReduced(dataCube, int(iarray[0]), int(iarray[1]))
			else:
				print 'Invalid rank or number of iterations.'
		else:
			print 'No Reduced Data Cube is loaded, please use the load command followed by the ReduceCube command.'
	elif userInput == 'pgnmfReducedCube' or userInput == 'pgr':
		if state[0] >= 1 and state[1] >= 1:
			i = raw_input('Please type rank and number of iterations. (int int)\n>')
			iarray = re.findall('\d+', i)
			if len(iarray) == 2:
				decomp.pgDataSetRandomReduced(dataCube, int(iarray[0]), int(iarray[1]))
			else:
				print 'Invalid rank or number of iterations.'
		else:
			print 'No Reduced Data Cube is loaded, please use the load command followed by the ReduceCube command.'
	elif userInput == 'interactiveCropDataCube' or userInput == 'icdc':
		if state[0] >= 1:
			interactiveCrop = rC.InteractiveCrop(dataCube.returnCompleteImage(), dataCube.returnCompleteSpectrum(), dataCube.eOrigin, dataCube.eScale)
			if interactiveCrop.exitFlag:
				res = dataCube.cropDataCube(interactiveCrop.y1, interactiveCrop.x1, interactiveCrop.y2, interactiveCrop.x2, interactiveCrop.ex1, interactiveCrop.ex2)
			else:
				res = False
			if res:
				print 'Success.'
			else:
				print 'Crop Failed.'
		else:
			print 'No Data Cube is loaded, please use the load command.'
	elif userInput == 'AtomFinder' or userInput == 'af':
		if state[0] >= 1:
			i = raw_input('Please type maxReductionSize and the possible peak divergence, and optionally the noise Area. (int int <int int int int>)\n>')
			iarray = re.findall('\d+', i)
			i = raw_input('Please input wether to use line profile (vertical or horizontal) or not. (H or V or N)\n>')
			iarray3 = re.findall('[a-zA-z]+', i)
			i = raw_input('Please type the energy of the peaks you wish to track [int]+\n>')
			iarray2 = re.findall('\d+', i)
			if len(iarray) == 2 and len(iarray2) > 0:
				ec = elementCompositions.ElementCompositions(dataCube)
				resd = ec.findAtomsEDS(int(iarray[0]), map(int, iarray2), int(iarray[1]), reductionMethodName=iarray3[0])
				i = raw_input('Would you like to print the results to a file? Yes/No\n>')
				if i == 'Yes' or i == 'y' or i == 'yes':
					i = raw_input('Please type the file name:\n>')
					if i != '':
						np.set_printoptions(threshold=np.nan)
						import pprint
						fw = open(i, 'w')
						fw.write(pprint.pformat(resd, indent=4))
						fw.close()
			elif len(iarray) == 6 and len(iarray2) > 0:
				ec = elementCompositions.ElementCompositions(dataCube)
				resd = ec.findAtomsEDS(int(iarray[0]), map(int, iarray2), int(iarray[1]), int(iarray[2]), int(iarray[3]), int(iarray[4]), int(iarray[5]), iarray3[0])
				i = raw_input('Would you like to print the results to a file? Yes/No\n>')
				if i == 'Yes' or i == 'y' or i == 'yes':
					i = raw_input('Please type the file name:\n>')
					if i != '':
						np.set_printoptions(threshold=np.nan)
						import pprint
						fw = open(i, 'w')
						fw.write(pprint.pformat(resd, indent=4))
						fw.close()
			else:
				print 'Invalid values.'
		else:
			print 'No Data Cube is loaded, please use the load command.'
	elif userInput == 'AtomProportions' or userInput == 'ap':
		if state[0] >= 1:
			i = raw_input('Please type maxReductionSize and the possible peak divergence, and optionally the noise Area. (int int <int int int int>)\n>')
			iarray = re.findall('\d+', i)
			i = raw_input('Please input wether to use line profile (vertical or horizontal) or not. (H or V or N)\n>')
			iarray3 = re.findall('[a-zA-z]+', i)
			i = raw_input('Please type the energy of the peaks you wish to track the proportions: [int]+\n>')
			iarray2 = re.findall('\d+', i)
			if len(iarray) == 2 and len(iarray2) > 0:
				ec = elementCompositions.ElementCompositions(dataCube)
				resd = ec.atomsProportionsEDS(int(iarray[0]), map(int, iarray2), int(iarray[1]), reductionMethodName=iarray3[0])
				i = raw_input('Would you like to print the results to a file? Yes/No\n>')
				if i == 'Yes' or i == 'y' or i == 'yes':
					i = raw_input('Please type the file name:\n>')
					if i != '':
						np.set_printoptions(threshold=np.nan)
						import pprint
						fw = open(i, 'w')
						fw.write(pprint.pformat(resd, indent=4))
						fw.close()
			elif len(iarray) == 6 and len(iarray2) > 0:
				ec = elementCompositions.ElementCompositions(dataCube)
				resd = ec.atomsProportionsEDS(int(iarray[0]), map(int, iarray2), int(iarray[1]), int(iarray[2]), int(iarray[3]), int(iarray[4]), int(iarray[5]), iarray3[0])
				i = raw_input('Would you like to print the results to a file? Yes/No\n>')
				if i == 'Yes' or i == 'y' or i == 'yes':
					i = raw_input('Please type the file name:\n>')
					if i != '':
						np.set_printoptions(threshold=np.nan)
						import pprint
						fw = open(i, 'w')
						fw.write(pprint.pformat(resd, indent=4))
						fw.close()
			else:
				print 'Invalid values.'
		else:
			print 'No Data Cube is loaded, please use the load command.'
	elif userInput == 'AtomProportonsGauss' or userInput == 'apg':
		if state[0] >= 1:
			i = raw_input('Please type maxReductionSize and the possible peak divergence, and optionally the noise Area. (int int <int int int int>)\n>')
			iarray = re.findall('\d+', i)
			i = raw_input('Please input wether to use line profile (vertical or horizontal) or not. (H or V or N)\n>')
			iarray3 = re.findall('[a-zA-z]+', i)
			i = raw_input('Please type the energy of the peaks you wish to track the proportions: [int]+\n>')
			iarray2 = re.findall('\d+', i)
			if len(iarray) == 2 and len(iarray2) > 0:
				ec = elementCompositions.ElementCompositions(dataCube)
				resd = ec.atomsProportionsEDS(int(iarray[0]), map(int, iarray2), int(iarray[1]), reductionMethodName=iarray3[0], gauss=True)
				i = raw_input('Would you like to print the results to a file? Yes/No\n>')
				if i == 'Yes' or i == 'y' or i == 'yes':
					i = raw_input('Please type the file name:\n>')
					if i != '':
						np.set_printoptions(threshold=np.nan)
						import pprint
						fw = open(i, 'w')
						fw.write(pprint.pformat(resd, indent=4))
						fw.close()
			elif len(iarray) == 6 and len(iarray2) > 0:
				ec = elementCompositions.ElementCompositions(dataCube)
				resd = ec.atomsProportionsEDS(int(iarray[0]), map(int, iarray2), int(iarray[1]), int(iarray[2]), int(iarray[3]), int(iarray[4]), int(iarray[5]), iarray3[0], gauss=True)
				i = raw_input('Would you like to print the results to a file? Yes/No\n>')
				if i == 'Yes' or i == 'y' or i == 'yes':
					i = raw_input('Please type the file name:\n>')
					if i != '':
						np.set_printoptions(threshold=np.nan)
						import pprint
						fw = open(i, 'w')
						fw.write(pprint.pformat(resd, indent=4))
						fw.close()
			else:
				print 'Invalid values.'
		else:
			print 'No Data Cube is loaded, please use the load command.'
	else:
		print 'Valid commands are {load, printStats, reduceCube, displaySpectrum, displaySpectrumAverage, displaySpectrumSum, dumpCube, dumpCubeReduction, setCalibrations, cropDataCube, displayImage, displayImageRange, displayImageRangeReduced, pgnmfDataCube, lnmfDataCube, pgnmfReducedCube, lnmfReducedCube, interactiveCropDataCube, atomFinder, atomProportions}'

	userInput = raw_input('>')
