import numpy as np 
import collections
import math
import cubeData as cd

####################################
#This is a Python adaptation of the Projected Gradient Non-Negative Matrix Factorization, originally made in Matlab by Chih-Jen Lin
#Original code and publication for the method can be found at: http://ntur.lib.ntu.edu.tw/bitstream/246246/20060927122855117716/1/pgradnmf.pdf
####################################


class PGNMF:

	def __init__(self):
		pass

	@staticmethod
	def doPgNMF(dataArray, Winit, Hinit, tol, timelimit, maxiter):
		#Parte antes do for
		W = np.copy(Winit)
		H = np.copy(Hinit)
		#initt = cputim
		gradW = W.dot(H.dot(H.T)) - dataArray.dot(H.T)
		gradH = W.T.dot(W).dot(H) - W.T.dot(dataArray)

		aux = np.concatenate((gradW, gradH.T))
		initgrad = math.sqrt(np.sum(aux * aux))
		print ('Init gradient norm {}'.format(initgrad))
		tolW = max(0.001, tol) * initgrad
		tolH = tolW

		#Parte pos for
		for i in xrange(1, maxiter + 1):
			#stopping condition
			faux = np.concatenate( ( gradW[np.logical_or(gradW < 0, W > 0)], gradH[(np.logical_or(gradH < 0, H > 0))] ) )
			projnorm = math.sqrt(np.sum(faux * faux))
			#TODO or cputime-initt > timelimit
			if projnorm < tol * initgrad:
				break

			#Chamada do nlssubprob
			res = PGNMF.nlssubprob(dataArray.T, H.T, W.T, tolW, 1000)
			W = np.copy(res.H)
			gradW = np.copy(res.grad)
			iterW = res.iter

			W = W.T
			gradW = gradW.T

			if iterW == 1:
				tolW *= 0.1

			#Chamada do nlssubprob
			res = PGNMF.nlssubprob(dataArray, W, H, tolH, 1000)
			H = np.copy(res.H)
			gradH = np.copy(res.grad)
			iterH = res.iter

			if iterH == 1:
				tolH *= 0.1
			if i % 10 == 0:
				print '.'

			#print 'passei'
		nmfResultTuple = collections.namedtuple('nmfResultTuple', ['W', 'H'])
		return nmfResultTuple(W, H)

	@staticmethod
	def nlssubprob(dataArray, W, Hinit, tol, maxiter):
		H = Hinit
		WtV = W.T.dot(dataArray)
		WtW = W.T.dot(W)
		alpha = 1
		beta = 0.1
		i = 0
		for i in xrange(1, maxiter + 1):
			grad = WtW.dot(H) - WtV
			gaux = grad[np.logical_or(grad < 0, H > 0)]
			#np.linalg.norm(gaux)
			projgrad = math.sqrt(np.sum(gaux * gaux))
			if projgrad < tol:
				break

			for inner_iter in xrange(1, 21):
				Hn = np.maximum(H - alpha * grad, 0)
				d = Hn - H
				gradd = np.sum(np.multiply(grad, d))
				dQd = np.sum(np.multiply( WtW.dot(d), d))
				suff_decr = (0.99 * gradd + 0.5 * dQd) < 0
				if inner_iter == 1:
					dec_alpha = not suff_decr
					Hp = np.copy(H)
				if dec_alpha:
					if suff_decr:
						H = np.copy(Hn)
						break
					else:
						alpha *= beta
				else:
					if (not suff_decr) or np.array_equal(Hp, Hn):
						H = np.copy(Hp)
						break
					else:
						alpha /= beta
						Hp = np.copy(Hn)
			if i == maxiter:
				print 'Max iter in nlssubprob'
		resTuple = collections.namedtuple('ResTuple', ['H', 'grad', 'iter'])
		return resTuple(H, grad, i)


def vectFunctionLesserThanGreaterThan(matA, matB, value):
	"""This is an auxiliaty funcion.
	It was created to simulate Matlab behavior, the behavior is (matrixCondition1 | matrixCondition2)
	or, more specifically, (matrix1 < 0 | matrix2 > 0), this should take two matrix and return 1 in the positions mat1 < 0 or mat2 >0.
	This function should be used only with np.vectorize!! It's semantic shold be:
	vfunc = np.vectorize(vectFunctionGreaterThan);
	vfunc(matrix1, matrix2, value)
	"""
	if matA > value or matB > value:
		return 1
	else:
		return 0

